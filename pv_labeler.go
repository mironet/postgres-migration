package main

import (
	"context"
	"fmt"
	"path/filepath"

	"github.com/sirupsen/logrus"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var (
	pvLabelerContainerName = "pv-labeler"
)

func runPvLabeler(ctx context.Context, k8sCli *K8sCli, migrationImage, serviceAccountName, dbSubPath string, pvc corev1.PersistentVolumeClaim) error {
	postgresUserID := int64(70)
	labelerPod := &corev1.Pod{
		ObjectMeta: metav1.ObjectMeta{
			Namespace: pvc.Namespace,
			Name:      fmt.Sprintf("pv-labeler-%s", pvc.Name),
			Labels:    pvc.Labels,
		},
		Spec: corev1.PodSpec{
			ServiceAccountName: serviceAccountName,
			RestartPolicy:      corev1.RestartPolicyNever,
			Volumes: []corev1.Volume{
				{
					VolumeSource: corev1.VolumeSource{
						PersistentVolumeClaim: &corev1.PersistentVolumeClaimVolumeSource{
							ClaimName: pvc.Name,
						},
					},
					Name: oldDbVolumeName,
				},
			},
			Containers: []corev1.Container{
				{
					Name:  pvLabelerContainerName,
					Image: migrationImage,
					Command: []string{
						"/app", commandNameLabelPV,
						fmt.Sprintf("--%s", flagNameNamespace), pvc.Namespace,
						fmt.Sprintf("--%s", flagNamePVCName), pvc.Name,
						fmt.Sprintf("--%s", flagNameOldPgDataPath), filepath.Join(oldMountPath, dbSubPath),
					},
					VolumeMounts: []corev1.VolumeMount{
						{
							Name:      oldDbVolumeName,
							MountPath: oldMountPath,
						},
					},
					SecurityContext: &corev1.SecurityContext{
						RunAsUser:  &postgresUserID,
						RunAsGroup: &postgresUserID,
					},
				},
			},
		},
	}

	// Make sure that the labeler pod does not exist yet.
	hasPod, err := k8sCli.HasPod(ctx, labelerPod.Name, labelerPod.Namespace, labelerPod.Labels)
	if err != nil {
		return fmt.Errorf("could not check whether labeler pod %s is already existing: %w", labelerPod.Name, err)
	}

	// If the labeler pod already exists, delete it.
	if hasPod {
		if err := k8sCli.cliSet.CoreV1().Pods(labelerPod.Namespace).Delete(ctx, labelerPod.Name, metav1.DeleteOptions{}); err != nil {
			return fmt.Errorf("could not delete outdated labeler pod %s: %w", labelerPod.Name, err)
		}
	}

	// Create the labeler pod (i.e. run the labeling).
	err = retryFunc("creating labeler pod", func() error {
		_, err := k8sCli.cliSet.CoreV1().Pods(labelerPod.Namespace).Create(ctx, labelerPod, metav1.CreateOptions{})
		return err
	})
	if err != nil {
		return fmt.Errorf("could not create labeler pod %s: %w", labelerPod.Name, err)
	}

	// Wait for the labeler pod to be done.
	if err := waitTillDone(ctx, k8sCli, *labelerPod, pvLabelerContainerName); err != nil {
		return fmt.Errorf("labeler pod %s did not finish successfully: %w", labelerPod.Name, err)
	}

	// Delete the labeler pod.
	if err := k8sCli.cliSet.CoreV1().Pods(labelerPod.Namespace).Delete(ctx, labelerPod.Name, metav1.DeleteOptions{}); err != nil && !errors.IsNotFound(err) {
		return fmt.Errorf("could not delete labeler pod %s: %w", labelerPod.Name, err)
	}
	return nil
}

func labelPV(ctx context.Context, args labelPVArgs) error {
	// Fetch the current postgres version.
	currentVersion, err := postgresVersion(args.oldPgDataPath)
	if err != nil {
		return fmt.Errorf("could not get current postgres version: %w", err)
	}
	logrus.Infof("current postgres version: %d", currentVersion)

	// Get a k8s client.
	k8sCli, err := k8sCli()
	if err != nil {
		return fmt.Errorf("could not get k8s client: %w", err)
	}

	// Get the pvc and pv.
	pvc, err := k8sCli.cliSet.CoreV1().PersistentVolumeClaims(args.namespace).Get(ctx, args.pvcName, metav1.GetOptions{})
	if err != nil {
		return fmt.Errorf("could not get pvc %s: %w", args.pvcName, err)
	}
	if pvc == nil {
		return fmt.Errorf("pvc pointer cannot be nil")
	}
	pv, err := k8sCli.cliSet.CoreV1().PersistentVolumes().Get(ctx, pvc.Spec.VolumeName, metav1.GetOptions{})
	if err != nil {
		return fmt.Errorf("could not get pv for pvc %s: %w", args.pvcName, err)
	}
	if pv == nil {
		return fmt.Errorf("pv for pvc %s cannot be nil", args.pvcName)
	}

	// Update the pv labels
	pvLabels := createPVLabels(*pvc, currentVersion)
	if pv.Labels == nil {
		pv.Labels = map[string]string{}
	}
	for k, v := range pvLabels {
		pv.Labels[k] = v
	}
	return retryFunc(fmt.Sprintf("updating labels for pv %s", pv.Name), func() error {
		_, err := k8sCli.cliSet.CoreV1().PersistentVolumes().Update(ctx, pv, metav1.UpdateOptions{})
		return err
	})
}
