package main

import (
	"fmt"
	"strconv"
	"strings"

	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
)

type PodDescriptorList []PodDescriptor

func (l PodDescriptorList) String() string {
	var sb strings.Builder
	sb.WriteString("[")
	for i, pod := range l {
		sb.WriteString(pod.GetName())
		if i < len(l)-1 {
			sb.WriteString(", ")
		}
	}
	sb.WriteString("]")
	return sb.String()
}

type PodDescriptor interface {
	GetNamespace() string
	GetName() string
	GetAnnotations() map[string]string
	GetLabels() map[string]string
	GetSpec() corev1.PodSpec
}

func postgresVersionFromPodSpec(podSpec corev1.PodSpec) (int, error) {
	postgresImageTag, err := postgresImageTagFromPodSpec(podSpec)
	if err != nil {
		return 0, err
	}
	return postgresVersionFromImageTag(postgresImageTag)
}

type SSTemplatePod struct {
	ss          appsv1.StatefulSet
	ordinal     int
	labels      map[string]string
	annotations map[string]string
}

func (p SSTemplatePod) GetNamespace() string {
	return p.ss.GetNamespace()
}

func (p SSTemplatePod) GetName() string {
	return fmt.Sprintf("%s-%d", p.ss.GetName(), p.ordinal)
}

func (p SSTemplatePod) GetAnnotations() map[string]string {
	// If annotations are set explicitly, use them.
	if p.annotations != nil {
		return p.annotations
	}
	return p.ss.Spec.Template.GetAnnotations()
}

func (p SSTemplatePod) GetLabels() map[string]string {
	// If labels are set explicitly, use them.
	if p.labels != nil {
		return p.labels
	}
	// Otherwise use the labels of the pod template.
	return p.ss.Spec.Template.GetLabels()
}

func (p SSTemplatePod) GetSpec() corev1.PodSpec {
	spec := p.ss.Spec.Template.Spec.DeepCopy()
	for _, pvc := range p.ss.Spec.VolumeClaimTemplates {
		// Create a new pvc volume from a volume claim template.
		newVolume := corev1.Volume{
			Name: pvc.Name,
			VolumeSource: corev1.VolumeSource{
				PersistentVolumeClaim: &corev1.PersistentVolumeClaimVolumeSource{
					ClaimName: fmt.Sprintf("%s-%s", pvc.Name, p.GetName()),
				},
			},
		}
		// If a volume with the same name already exists, replace it.
		doPrependVolume := true
		for i := range spec.Volumes {
			if spec.Volumes[i].Name == newVolume.Name {
				spec.Volumes[i] = newVolume
				doPrependVolume = false
				break
			}
		}
		// Otherwise prepend the new volume (because that is what k8s also does
		// apparently).
		if doPrependVolume {
			spec.Volumes = append([]corev1.Volume{newVolume}, spec.Volumes...)
		}
	}
	spec.Hostname = p.GetName()
	spec.Subdomain = p.ss.Spec.ServiceName
	return *spec
}

var _ PodDescriptor = SSTemplatePod{}

type CoreV1Pod struct {
	*corev1.Pod
}

func (p CoreV1Pod) GetSpec() corev1.PodSpec {
	return p.Spec
}

var _ PodDescriptor = CoreV1Pod{}

type PVCDescriptor struct {
	// The name of the pvc this descriptor describes.
	pvcName string
	// The namespace of the pvc this descriptor describes.
	pvcNamespace string
	// The postgres version of the pvc this descriptor describes. Optional: Is 0
	// if not set
	postgresVersion int
	// The db pod that uses the pvc this descriptor describes. Optional: It can
	// be nil, if no information about the db pod is available (in case the
	// associated pod and statefulset do not exist). If the associated
	// statefulset exists dbPod will be of type SSTemplatePod. If the associated
	// statefulset does not exist but the associated pod exists, dbPod will be
	// of type CoreV1Pod.
	dbPod PodDescriptor
	// The app pod that uses the db pod. Optional: It can be nil, if no
	// information about the app pod is available (in case the associated pod
	// and statefulset do not exist). If the associated statefulset exists
	// appPod will be of type SSTemplatePod. If the associated statefulset does
	// not exist but the associated pod exists, appPod will be of type
	// CoreV1Pod.
	appPod PodDescriptor
}

func (d PVCDescriptor) String() string {
	return fmt.Sprintf(
		"{pvc namespace: %s, pvc name: %s, postgres version: %d, dbPod: %T, appPod: %T}",
		d.pvcNamespace, d.pvcName, d.postgresVersion, d.dbPod, d.appPod,
	)
}
func (d PVCDescriptor) PostgresVersion() (int, error) {
	if d.dbPod != nil {
		return postgresVersionFromPodSpec(d.dbPod.GetSpec())
	}
	return d.postgresVersion, nil
}

func (d PVCDescriptor) GetName() string {
	if d.dbPod != nil {
		return fmt.Sprintf("%s-%s", dataVolumeName, d.dbPod.GetName())
	}
	return d.pvcName
}

func (d PVCDescriptor) GetNamespace() string {
	if d.dbPod != nil {
		return d.dbPod.GetNamespace()
	}
	return d.pvcNamespace
}

func getSSName(pod PodDescriptor) string {
	return trimOrdinal(pod.GetName())
}

func (d PVCDescriptor) GetAppSSName() (string, error) {
	if d.appPod == nil {
		return "", fmt.Errorf("we don't know the app statefulset name")
	}
	return getSSName(d.appPod), nil
}

func (d PVCDescriptor) GetDbSSName() (string, error) {
	if d.dbPod == nil {
		return "", fmt.Errorf("we don't know the db statefulset name")
	}
	return getSSName(d.dbPod), nil
}

func trimOrdinal(name string) string {
	if !strings.Contains(name, "-") {
		return name
	}
	parts := strings.Split(name, "-")
	// This should never happen if the separator is not empty.
	if len(parts) == 0 {
		return name
	}
	// If the last part is not an integer, we return the input string.
	_, err := strconv.Atoi(parts[len(parts)-1])
	if err != nil {
		return name
	}
	return strings.Join(parts[:len(parts)-1], "-")
}
