package main

import (
	"testing"
)

func Test_postgresVersionFromImageTag(t *testing.T) {
	tests := []struct {
		name    string
		tag     string
		want    int
		wantErr bool
	}{

		{
			name:    "success",
			tag:     "11.9-alpine",
			want:    11,
			wantErr: false,
		},
		{
			name:    "success (only major version)",
			tag:     "16-bullseye",
			want:    16,
			wantErr: false,
		},
		{
			name:    "fail",
			tag:     "latest",
			want:    0,
			wantErr: true,
		},
		{
			name:    "fail",
			tag:     "bullseye",
			want:    0,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := postgresVersionFromImageTag(tt.tag)
			if (err != nil) != tt.wantErr {
				t.Errorf("postgresVersionFromImageTag() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("postgresVersionFromImageTag() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getOrdinal(t *testing.T) {

	tests := []struct {
		name    string
		want    int
		wantErr bool
	}{
		{
			name:    "fail",
			want:    0,
			wantErr: true,
		},
		{
			name:    "success-0",
			want:    0,
			wantErr: false,
		},
		{
			name:    "success-48465312",
			want:    48465312,
			wantErr: false,
		},
		{
			name:    "suc-cess-14",
			want:    14,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := getOrdinal(tt.name)
			if (err != nil) != tt.wantErr {
				t.Errorf("getOrdinal() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("getOrdinal() = %v, want %v", got, tt.want)
			}
		})
	}
}
