IMAGE_NAME=gitlab.com/mironet/postgres-migration
DOCKER_FILE=docker/Dockerfile
COMPOSE_FILE=docker/docker-compose.yml
POSTGRES_COMPOSE_FILE=docker/docker-compose-postgres.yml
POSTGRES_NEW_COMPOSE_FILE=docker/docker-compose-postgres-new.yml
PKG_LIST=`go list ./... | grep -v /vendor/`

# HELP
# This will output the help for each task
# thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

up: ## Run docker image for manual testing.
	docker-compose -f $(COMPOSE_FILE) up

down: ## Remove running docker containers.
	docker-compose -f $(COMPOSE_FILE) down

up-postgres: ## Run postgres docker image for manual testing.
	docker-compose -f $(POSTGRES_COMPOSE_FILE) up

down-postgres: ## Remove postgres running docker containers.
	docker-compose -f $(POSTGRES_COMPOSE_FILE) down

up-postgres-new: ## Run postgres docker image for manual testing.
	docker-compose -f $(POSTGRES_NEW_COMPOSE_FILE) up

down-postgres-new: ## Remove postgres running docker containers.
	docker-compose -f $(POSTGRES_NEW_COMPOSE_FILE) down

clean-postgres-volumes: ## Clean all postgres volumes.
	docker volume rm -f docker_postgres-archive || exit 0
	docker volume rm -f docker_postgres-data || exit 0
	docker volume rm -f docker_postgres-new-data || exit 0

build-docker: export DOCKER_BUILDKIT = 1
build-docker: ## Build the Docker image.
	docker build --build-arg APP_VERSION=$$(git describe --always) -t $(IMAGE_NAME) -f $(DOCKER_FILE) .

test: ## Execute all tests.
	go test -race $(PKG_LIST) -count 1
