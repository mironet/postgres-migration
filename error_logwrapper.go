package main

import (
	"context"
	"errors"

	"github.com/cenkalti/backoff/v4"
	"github.com/sirupsen/logrus"
)

// errSuccess is an error wrapper which only logs the embedded error but should
// otherwise not be considered as one. It is used if an action isn't performed,
// but not because of an error.
type errSuccess struct {
	error
}

func (e errSuccess) Unwrap() error {
	return e.error
}

func errorLogWrapper(taskName string, retryFunc func() error) func() error {
	return func() error {
		err := retryFunc()
		if err != nil {
			// Stop retrying if the context was canceled.
			if errors.Is(err, context.Canceled) {
				return backoff.Permanent(err)
			}
			var e errSuccess
			if errors.As(err, &e) {
				logrus.Infof("not %s: %v", taskName, err)
				return nil
			}
			// Only log error message if the error is not a permanent backoff
			// error on the outside.
			var er *backoff.PermanentError
			if !errors.As(err, &er) {
				logrus.Debugf("error %s: %v, retrying...", taskName, err)
			}
		} else {
			logrus.Infof("👍 success: %s", taskName)
		}
		return err
	}
}
