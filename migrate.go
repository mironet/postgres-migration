package main

import (
	"fmt"
	"math"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/sirupsen/logrus"
)

const (
	pgVersionFileName = "PG_VERSION"
	pgUpgradeExecName = "pg_upgrade"
	initDbExecName    = "initdb"
	libPathTemplate   = "/usr/lib/postgresql/%d/lib"
	binPathTemplate   = "/usr/lib/postgresql/%d/bin"
	dbEncoding        = "UTF-8"
	dbLocale          = "en_US.UTF-8"
	hba_file          = "pg_hba.conf"
)

var supportedVersions = []int{11, 12, 13, 14, 15}

// TODO Collect some metrics during/after migration.

func migrate(args migrateArgs) error {
	// Fetch the current postgres version.
	currentVersion, err := postgresVersion(args.oldPgDataPath)
	if err != nil {
		return fmt.Errorf("could not get current postgres version: %w", err)
	}
	logrus.Infof("current postgres version: %d", currentVersion)

	// If the current version matches the target version, no migration is
	// required.
	if currentVersion == args.to {
		logrus.Infof("no migration required: current version already matches target version %d", args.to)
		return nil
	}

	// Validate from- and to-versions.
	if err := validateFromTo(currentVersion, args.to); err != nil {
		return err
	}

	oldbindir := fmt.Sprintf(binPathTemplate, currentVersion)
	logrus.Infof("old bin dir: %s", oldbindir)
	newbindir := fmt.Sprintf(binPathTemplate, args.to)
	logrus.Infof("new bin dir: %s", newbindir)

	// Create new postgres data path, if it does not exist yet.
	if err := os.Mkdir(args.newPgDataPath, os.ModePerm); err != nil && !os.IsExist(err) {
		return fmt.Errorf("could not create directory %s: %w", args.newPgDataPath, err)
	}

	// Initialize the new database.
	if err := runCommand(exec.Command(
		filepath.Join(newbindir, initDbExecName),
		"--pgdata", args.newPgDataPath,
		"-E", dbEncoding,
		fmt.Sprintf("--lc-collate=%s", dbLocale),
		fmt.Sprintf("--lc-ctype=%s", dbLocale),
	)); err != nil {
		return err
	}

	// Run pg_upgrade in check-mode.
	if err := runCommand(exec.Command(
		filepath.Join(newbindir, pgUpgradeExecName),
		"-b", oldbindir,
		"-B", newbindir,
		"-d", args.oldPgDataPath,
		"-D", args.newPgDataPath,
		"--check",
	)); err != nil {
		return err
	}

	// Run pg_upgrade properly.
	if err := runCommand(exec.Command(
		filepath.Join(newbindir, pgUpgradeExecName),
		"-b", oldbindir,
		"-B", newbindir,
		"-d", args.oldPgDataPath,
		"-D", args.newPgDataPath,
	)); err != nil {
		return err
	}

	// Copy pg_hba.conf file which is backup by migration pod init container
	if err := runCommand(exec.Command(
		"cp",
		filepath.Join(newMountPath, hba_file),
		filepath.Join(args.newPgDataPath, hba_file),
	)); err != nil {
		return err
	}

	return verifyMigrationSuccess(args)
}

func verifyMigrationSuccess(args migrateArgs) error {
	// Fetch the current postgres version.
	currentVersion, err := postgresVersion(args.newPgDataPath)
	if err != nil {
		return fmt.Errorf("could not get current postgres version: %w", err)
	}

	// Make sure it corresponds to the target version.
	if currentVersion != args.to {
		return fmt.Errorf("current version %d not corresponding to target version %d", currentVersion, args.to)
	}

	// TODO Do we need to verify further things?

	return nil
}

func min(elems []int) int {
	min := math.MaxInt
	for _, e := range elems {
		if e < min {
			min = e
		}
	}
	return min
}

func max(elems []int) int {
	max := math.MinInt
	for _, e := range elems {
		if e > max {
			max = e
		}
	}
	return max
}

func validateFromTo(from, to int) error {
	if from >= to {
		return fmt.Errorf("to-version %d must be greater than from-version %d", to, from)
	}
	minSupportedVersion := min(supportedVersions)
	maxSupportedVersion := max(supportedVersions)
	if from < minSupportedVersion || from >= maxSupportedVersion {
		return fmt.Errorf("invalid from-version %d: must be >=%d and <%d", from, minSupportedVersion, maxSupportedVersion)
	}
	if to <= minSupportedVersion || to > maxSupportedVersion {
		return fmt.Errorf("invalid to-version %d: must be >%d and <=%d", to, minSupportedVersion, maxSupportedVersion)
	}
	return nil
}

func postgresVersion(dataDir string) (int, error) {
	bytes, err := os.ReadFile(filepath.Join(dataDir, pgVersionFileName))
	if err != nil {
		return 0, fmt.Errorf("could not read pg version file: %w", err)
	}
	str := string(bytes)
	str = strings.TrimSuffix(str, "\n")
	return strconv.Atoi(str)
}

func runCommand(cmd *exec.Cmd) error {
	if cmd == nil {
		return fmt.Errorf("command pointer cannot be nil")
	}
	logrus.Infof("command is %s", cmd.String())
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Dir = "/tmp"
	if err := cmd.Run(); err != nil {
		return fmt.Errorf("error running command %s: %w", cmd.String(), err)
	}
	logrus.Infof("successfully ran command %s", cmd.String())
	return nil
}
