package main

import (
	"context"
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/selection"
)

const (
	postgresImageRepository = "postgres"
)

func migrator(ctx context.Context, args migratorArgs) error {
	// Get a k8s client.
	k8sCli, err := k8sCli()
	if err != nil {
		return fmt.Errorf("could not get k8s client: %w", err)
	}

	// Find the pod we are running in (to extract the proper postgres-migration
	// image and service account name to use below).
	migrationImage, serviceAccountName, err := fetchOwnImage(ctx, k8sCli, args)
	if err != nil {
		return fmt.Errorf("could not get the image name of the pod we are running in: %w", err)
	}
	logrus.Infof("successfully fetched migration image %s", migrationImage)

	// Migrate author, public & shared db components one after another (if required).
	argsList := []migrateModeArgs{
		{
			magnoliaMode:  "author",
			dbName:        args.authorDBName,
			appComponent:  instanceTypeAuthor,
			targetVersion: args.authorTo,
			targetTag:     args.authorTag,
			dbSubPath:     args.authorDBSubPath,
		},
		{
			magnoliaMode:  "public",
			dbName:        args.publicDBName,
			appComponent:  instanceTypePublic,
			targetVersion: args.publicTo,
			targetTag:     args.publicTag,
			dbSubPath:     args.publicDBSubPath,
		},
		{
			magnoliaMode:  "sharedDB",
			dbName:        args.sharedDBName,
			appComponent:  "",
			targetVersion: args.sharedTo,
			targetTag:     args.sharedTag,
			dbSubPath:     args.sharedDBSubPath,
		},
	}
	for _, migrateModeArgs := range argsList {
		if err := migrateMode(ctx, k8sCli, migrationImage, serviceAccountName, args.namespace, args.app, args.release, migrateModeArgs); err != nil {
			return err
		}
	}

	return nil
}

type migrateModeArgs struct {
	magnoliaMode  string
	dbName        string
	appComponent  string
	targetVersion int
	targetTag     string
	dbSubPath     string
}

func migrateMode(ctx context.Context, k8sCli *K8sCli, migrationImage, serviceAccountName, namespace, app, release string, args migrateModeArgs) error {
	if args.dbName != "" {
		// List all pvcs for the current magnolia mode.
		pvcLabels := map[string]string{
			labelKeyApp:       app,
			labelKeyRelease:   release,
			labelKeyComponent: args.dbName,
			labelKeyTier:      databaseTier,
		}
		pvcs, err := k8sCli.dataPVCList(ctx, namespace, pvcLabels)
		if err != nil {
			return fmt.Errorf("could not get data pvc list for %s db: %w", args.magnoliaMode, err)
		}

		// Generate the stateful set names for the app and the db (if required)
		// in the same way as they are generated in the magnolia-helm chart.
		dbSSName := fmt.Sprintf("%s-%s-db", app, args.dbName)
		var appSSName string
		if args.magnoliaMode != "sharedDB" {
			appSSName = fmt.Sprintf("%s-%s", app, args.magnoliaMode)
		}

		// Find all pvc descriptors for the current magnolia mode.
		pvcDescriptorList := make([]PVCDescriptor, 0, len(pvcs))
		for _, pvc := range pvcs {
			desc, err := constructPVCDescriptor(ctx, k8sCli, migrationImage, serviceAccountName, namespace, dbSSName, appSSName, args.dbSubPath, pvc)
			if err != nil {
				return fmt.Errorf("could not construct pvc descriptor for pvc %s: %w", pvc.Name, err)
			}
			pvcDescriptorList = append(pvcDescriptorList, desc)
			logrus.Infof("found pvc descriptor %s", desc)
		}

		// Migrate each pvc for the current magnolia mode.
		for _, pvcDescriptor := range pvcDescriptorList {
			logrus.Infof("start migration for pvc %s", pvcDescriptor.pvcName)
			if err := migrateByPVCDescriptor(ctx, k8sCli, pvcDescriptor, args.targetVersion, args.targetTag, migrationImage, args.dbSubPath); err != nil {
				return fmt.Errorf("could not migrate %s db: %w", args.magnoliaMode, err)
			}
			logrus.Infof("successfully migrated pvc %s", pvcDescriptor.pvcName)
		}

		logrus.Infof("successfully migrated %s dbs", args.magnoliaMode)
	}

	return nil
}

func fetchStatefulSets(ctx context.Context, k8sCli *K8sCli, namespace, release, appComponent, dbComponent string) (*appsv1.StatefulSet, *appsv1.StatefulSet, error) {
	var appStatefulSet *appsv1.StatefulSet
	if appComponent != "" {
		appSSList, err := k8sCli.StatefulSetList(ctx, []string{appComponent}, []string{appTier}, namespace, release)
		if err != nil {
			return nil, nil, fmt.Errorf("could not fetch app stateful set: %w", err)
		}
		if len(appSSList.Items) != 1 {
			return nil, nil, fmt.Errorf("expected exactly one app stateful set, but got %d", len(appSSList.Items))
		}
		appStatefulSet = &appSSList.Items[0]
	}

	databaseSSList, err := k8sCli.StatefulSetList(ctx, []string{dbComponent}, []string{databaseTier}, namespace, release)
	if err != nil {
		return nil, nil, fmt.Errorf("could not fetch db stateful set: %w", err)
	}
	if len(databaseSSList.Items) != 1 {
		return nil, nil, fmt.Errorf("expected exactly one db stateful set, but got %d", len(databaseSSList.Items))
	}
	return appStatefulSet, &databaseSSList.Items[0], nil
}

func getOrdinal(name string) (int, error) {
	parts := strings.Split(name, "-")
	// This should never happen if the separator is not empty.
	if len(parts) == 0 {
		return 0, fmt.Errorf("strings.Split failed: something is very wrong")
	}
	ordinal, err := strconv.Atoi(parts[len(parts)-1])
	if err != nil {
		return 0, fmt.Errorf("last part of %s is not an integer: %w", name, err)
	}
	return ordinal, nil
}

func findBestPodDescriptor(ctx context.Context, k8sCli *K8sCli, namespace, ssName string, ordinal int) PodDescriptor {
	podName := fmt.Sprintf("%s-%d", ssName, ordinal)
	pod, getPodErr := k8sCli.GetPodWithRetry(ctx, namespace, podName, 5*time.Second)
	ss, getSSErr := k8sCli.GetStatefulSetWithRetry(ctx, namespace, ssName, 5*time.Second)

	switch {
	// We prefer a pod descriptor using the statefil set template over all other
	// pod descriptors.
	case getSSErr == nil:
		var labels, annotations map[string]string
		if getPodErr == nil && pod != nil {
			// But we use the labels & annotations of the existing pod if possible.
			labels = pod.GetLabels()
			annotations = pod.GetAnnotations()
		}
		return SSTemplatePod{
			ss:          *ss,
			ordinal:     ordinal,
			labels:      labels,
			annotations: annotations,
		}

	// If we could not find the statefulset, we fall back to the core v1 pod
	// descriptor.
	case getPodErr == nil:
		return CoreV1Pod{
			pod,
		}

	// If we could find neither the statefulset nor the pod, we return a nil PodDescriptor.
	default:
		return nil
	}
}

var ErrPGLabelNotFound = fmt.Errorf("postgres label not found")

func fetchVersionFromPVLabel(ctx context.Context, k8sCli *K8sCli, pvc corev1.PersistentVolumeClaim) (int, error) {
	pvName := pvc.Spec.VolumeName
	// Get the persistent volume.
	pv, err := k8sCli.cliSet.CoreV1().PersistentVolumes().Get(ctx, pvName, metav1.GetOptions{})
	if err != nil {
		return 0, fmt.Errorf("could not get persistent volume %s: %w", pvName, err)
	}
	if pv == nil {
		return 0, fmt.Errorf("persistent volume pointer cannot be nil")
	}

	// If the persistent volume has a postgres-version label, we use it.
	strVersion, ok := pv.Labels[labelKeyPGVersion]
	if ok {
		version, err := strconv.Atoi(strVersion)
		if err != nil {
			return 0, fmt.Errorf("could not convert postgres version %s to integer: %w", strVersion, err)
		}
		return version, nil
	}

	return 0, ErrPGLabelNotFound
}

func fetchPVCVersion(ctx context.Context, k8sCli *K8sCli, migrationImage, serviceAccountName, dbSubPath string, pvc corev1.PersistentVolumeClaim) (int, error) {
	version, err := fetchVersionFromPVLabel(ctx, k8sCli, pvc)
	if err == nil {
		return version, nil
	}
	if !errors.Is(err, ErrPGLabelNotFound) {
		return 0, fmt.Errorf("could not fetch postgres version from pv label: %w", err)
	}

	// Otherwise we need to label the persistent volume first.
	logrus.Infof("did not find postgres version label in pv of pvc %s: running pv labeler", pvc.Name)
	if err := runPvLabeler(ctx, k8sCli, migrationImage, serviceAccountName, dbSubPath, pvc); err != nil {
		return 0, fmt.Errorf("could not run pv labeler for %s: %w", pvc.Spec.VolumeName, err)
	}
	logrus.Infof("successfully ran pv labeler for pv of pvc %s", pvc.Name)

	return fetchVersionFromPVLabel(ctx, k8sCli, pvc)
}

func constructPVCDescriptor(ctx context.Context, k8sCli *K8sCli, migrationImage, serviceAccountName, namespace, dbSSName, appSSName, dbSubPath string, pvc corev1.PersistentVolumeClaim) (PVCDescriptor, error) {
	ordinal, err := getOrdinal(pvc.Name)
	if err != nil {
		return PVCDescriptor{}, fmt.Errorf("could not get ordinal for pvc %s: %w", pvc.Name, err)
	}

	// Get the pod descriptors for the current ordinal.
	dbPod := findBestPodDescriptor(ctx, k8sCli, namespace, dbSSName, ordinal)
	var appPod PodDescriptor
	if appSSName != "" {
		appPod = findBestPodDescriptor(ctx, k8sCli, namespace, appSSName, ordinal)
	}

	var currentVersion int
	if dbPod == nil {
		// Fetch the pvc current version.
		var err error
		currentVersion, err = fetchPVCVersion(ctx, k8sCli, migrationImage, serviceAccountName, dbSubPath, pvc)
		if err != nil {
			return PVCDescriptor{}, fmt.Errorf("could not fetch postgres version for pvc %s: %w", pvc.Name, err)
		}
	}

	return PVCDescriptor{
		pvcName:         pvc.Name,
		pvcNamespace:    pvc.Namespace,
		postgresVersion: currentVersion,
		dbPod:           dbPod,
		appPod:          appPod,
	}, nil
}

func migrateByPVCDescriptor(ctx context.Context, k8sCli *K8sCli, pvc PVCDescriptor, targetVersion int, targetTag, migrationImage, dbSubPath string) error {
	// Fetch the current postgres version.
	currentVersion, err := pvc.PostgresVersion()
	if err != nil {
		return fmt.Errorf("could not get current postgres version for pvc %s: %w", pvc.GetName(), err)
	}

	// Check whether the migration is required.
	isRequired, reason, err := isRequired(currentVersion, targetVersion)
	if err != nil {
		return fmt.Errorf("could not check whether migration is required for %s: %w", pvc.GetName(), err)
	}
	if !isRequired {
		logrus.Infof("no migration required for %s: %s", pvc.GetName(), reason)
		return nil
	}
	logrus.Infof("migration required for %s: %s", pvc.GetName(), reason)

	// Delete stateful sets (if required), but not their pods.
	if pvc.dbPod != nil {
		dbSSName, err := pvc.GetDbSSName()
		if err != nil {
			return fmt.Errorf("could not get db stateful set name for pvc %s: %w", pvc.GetName(), err)
		}
		if err := k8sCli.DeleteStatfulSetWithOrphanedPods(ctx, pvc.GetNamespace(), dbSSName); err != nil {
			return fmt.Errorf("could not delete db stateful set %s: %w", dbSSName, err)
		}
	}
	if pvc.appPod != nil {
		appSSName, err := pvc.GetAppSSName()
		if err != nil {
			return fmt.Errorf("could not get app stateful set name for pvc %s: %w", pvc.GetName(), err)
		}
		if err := k8sCli.DeleteStatfulSetWithOrphanedPods(ctx, pvc.GetNamespace(), appSSName); err != nil {
			return fmt.Errorf("could not delete app stateful set %s: %w", appSSName, err)
		}
	}

	// Create a new migration pod
	migrationPod, err := NewMigrationPod(ctx, k8sCli, pvc, currentVersion, targetVersion, targetTag, migrationImage, dbSubPath)
	if err != nil {
		return fmt.Errorf("could not create new migration pod for pvc %s: %w", pvc.GetName(), err)
	}

	// Prepare the migration.
	if err := migrationPod.Prepare(); err != nil {
		return fmt.Errorf("could not prepare migration pod for pvc %s: %w", pvc.GetName(), err)
	}

	// Delete the db (and app) pod(s).
	if err := migrationPod.DeletePods(); err != nil {
		return fmt.Errorf("could not delete db (and app) pod(s) before migration for pvc %s: %w", pvc.GetName(), err)
	}

	// Run the migration.
	logrus.Infof("starting migration for pvc %s", pvc.GetName())
	if err := migrationPod.Run(); err != nil {
		return fmt.Errorf("could not run migration pod for pvc %s: %w", pvc.GetName(), err)
	}

	// Switch volumes after the successful migration.
	if err := migrationPod.SwitchVolumes(); err != nil {
		return fmt.Errorf("could not switch volumes for pvc %s: %w", pvc.GetName(), err)
	}
	logrus.Infof("successfully migrated db for pvc %s", pvc.GetName())

	// Recreate the db (and app) pod(s) again. The db pod now with the new
	// database tag.
	if err := migrationPod.CreatePods(); err != nil {
		return fmt.Errorf("could not create db (and app) pod(s) after migration for pvc %s: %w", pvc.GetName(), err)
	}
	return nil
}

// fetchOwnImage returns the image name and the service account of the pod we
// are running in or an error.
func fetchOwnImage(ctx context.Context, cli *K8sCli, args migratorArgs) (string, string, error) {
	if cli == nil {
		return "", "", fmt.Errorf("kubernetes client pointer cannot be nil")
	}

	// Construct label selector list option.
	releaseReq, err := labels.NewRequirement(labelKeyRelease, selection.Equals, []string{args.release})
	if err != nil {
		return "", "", fmt.Errorf("could not construct release requirement: %w", err)
	}
	tierReq, err := labels.NewRequirement(labelKeyTier, selection.Equals, []string{dbMigrationTier})
	if err != nil {
		return "", "", fmt.Errorf("could not construct tier requirement: %w", err)
	}
	labelSelector := labels.NewSelector().Add(*releaseReq, *tierReq).String()
	opts := metav1.ListOptions{
		LabelSelector: labelSelector,
	}

	var imageName, serviceAccountName string
	err = retryFunc("fetching own image", func() error {
		podList, err := cli.cliSet.CoreV1().Pods(args.namespace).List(ctx, opts)
		if err != nil {
			return fmt.Errorf("could not get pod list: %w", err)
		}
		if len(podList.Items) == 0 {
			return fmt.Errorf("found no pods with label selector %s", labelSelector)
		}
		for _, pod := range podList.Items {
			logrus.Infof("found pod %s in phase %s with %d containers", pod.Name, pod.Status.Phase, len(pod.Spec.Containers))
			if strings.HasPrefix(pod.Name, args.jobName) &&
				pod.Status.Phase == corev1.PodRunning {
				serviceAccountName = pod.Spec.ServiceAccountName
				for i, container := range pod.Spec.Containers {
					if container.Name == commandNameMigrator {
						imageName = pod.Spec.Containers[i].Image
						return nil
					}
				}
			}
		}
		return fmt.Errorf("no running pod with prefix %s in name found using label selector %s", args.jobName, labelSelector)
	})
	return imageName, serviceAccountName, err
}

func postgresImageTagFromPodSpec(podSpec corev1.PodSpec) (string, error) {
	postgresImageTag := ""
	for _, container := range podSpec.Containers {
		if strings.HasPrefix(container.Image, postgresImageRepository) {
			parts := strings.Split(container.Image, ":")
			if len(parts) != 2 {
				return "", fmt.Errorf("expected postgres image %s to have 2 parts (when spliting by \":\") but got %d", container.Image, len(parts))
			}
			if postgresImageTag != "" {
				// Here we know that there is more than one postgres container.
				return "", fmt.Errorf("expected only one postgres container in db pod spec, but got at least 2")
			}
			postgresImageTag = parts[1]
		}
	}
	if postgresImageTag == "" {
		return "", fmt.Errorf("no postgres container in db pod")
	}
	return postgresImageTag, nil
}

func postgresVersionFromImageTag(tag string) (int, error) {
	if tag == "" {
		return 0, fmt.Errorf("postgres tag cannot be empty")
	}
	if tag == "latest" {
		return 0, fmt.Errorf("postgres tag cannot be latest")
	}
	parts := strings.Split(tag, ".")
	pgVersion, err := strconv.Atoi(parts[0])
	if err != nil {
		// Try to recover by spliting by hyphen separator. (Some postgres images
		// are tagged like "16-bullseye")
		parts := strings.Split(tag, "-")
		pgVersion, err = strconv.Atoi(parts[0])
		if err != nil {
			return 0, fmt.Errorf("could not extract postgres version from tag %s", tag)
		}
	}
	return pgVersion, nil
}

// isRequired returns whether the migration is required (as bool), a reason why
// it is required (or not required, respectively) and an error.
func isRequired(haveVersion, wantVersion int) (bool, string, error) {
	// If the current version matches the target version, no migration is
	// required.
	if haveVersion == wantVersion {
		return false, fmt.Sprintf("current version already matches target version %d", wantVersion), nil
	}

	// Validate from- and to-versions.
	if err := validateFromTo(haveVersion, wantVersion); err != nil {
		return false, "migration necessity could not be determined", err
	}

	// Here we know that the current version does not match the target version
	// and that the input is valid. Thus we conclude that a migration is
	// required.
	return true, fmt.Sprintf("current version %d below target version %d", haveVersion, wantVersion), nil
}
