package main

import (
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/cenkalti/backoff/v4"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/selection"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

const (
	labelKeyComponent = "component"
	labelKeyRelease   = "release"
	labelKeyTier      = "tier"
	labelKeyApp       = "app"

	instanceTypeAuthor = "author-instance"
	instanceTypePublic = "public-instance"
	appTier            = "app"
	databaseTier       = "database"
	dbMigrationTier    = "db-migration"

	dataVolumeName = "data"

	propagationPolicyOrphan = metav1.DeletePropagationOrphan
)

var (
	dataVolumeClaimPrefix = fmt.Sprintf("%s-", dataVolumeName)
)

type K8sCli struct {
	cliSet *kubernetes.Clientset
}

func k8sCli() (*K8sCli, error) {
	config, err := rest.InClusterConfig()
	if err != nil {
		return nil, fmt.Errorf("could not create k8s in-cluster config: %w", err)
	}
	cliSet, err := kubernetes.NewForConfig(config)
	return &K8sCli{cliSet: cliSet}, err
}

type getLabeledResourceFunc func() (labelGetter, error)

type labelGetter interface {
	GetLabels() map[string]string
}

func (cli *K8sCli) hasResource(name string, get getLabeledResourceFunc, wantLabels map[string]string) (bool, error) {
	labeledResource, err := get()
	if errors.IsNotFound(err) {
		return false, nil
	}
	if err != nil {
		return false, fmt.Errorf("could not get resource %s: %w", name, err)
	}
	// Match all labels.
	haveLabels := labeledResource.GetLabels()
	for k, v := range wantLabels {
		if haveLabels[k] != v {
			return false, nil
		}
	}
	return true, nil
}

func (cli *K8sCli) HasPod(ctx context.Context, name, namespace string, wantLabels map[string]string) (bool, error) {
	getFunc := func() (labelGetter, error) {
		return cli.cliSet.CoreV1().Pods(namespace).Get(ctx, name, metav1.GetOptions{})
	}
	return cli.hasResource(name, getFunc, wantLabels)
}

func (cli *K8sCli) HasPVC(ctx context.Context, name, namespace string, wantLabels map[string]string) (bool, error) {
	getFunc := func() (labelGetter, error) {
		return cli.cliSet.CoreV1().PersistentVolumeClaims(namespace).Get(ctx, name, metav1.GetOptions{})
	}
	return cli.hasResource(name, getFunc, wantLabels)

}

func (cli *K8sCli) dataPVCList(ctx context.Context, namespace string, dataPVCLabels map[string]string) ([]corev1.PersistentVolumeClaim, error) {
	if len(dataPVCLabels) == 0 {
		return nil, fmt.Errorf("given data pvc labels are empty")
	}
	selector := labels.SelectorFromSet(dataPVCLabels).String()
	list, err := cli.cliSet.CoreV1().PersistentVolumeClaims(namespace).List(ctx, metav1.ListOptions{
		LabelSelector: selector,
	})
	if err != nil {
		return nil, err
	}
	if list == nil {
		return nil, fmt.Errorf("pvc list cannot be nil")
	}
	outList := make([]corev1.PersistentVolumeClaim, 0)
	for _, elem := range list.Items {
		if strings.HasPrefix(elem.Name, dataVolumeClaimPrefix) {
			outList = append(outList, elem)
		}
	}
	if len(outList) == 0 {
		return nil, fmt.Errorf("did not find data pvcs using selector %s", selector)
	}
	return outList, nil
}

func (cli *K8sCli) sortedPodListFromStatefulset(ctx context.Context, ss appsv1.StatefulSet) (PodDescriptorList, error) {
	numWantPods := int32(1)
	if ss.Spec.Replicas != nil {
		numWantPods = *ss.Spec.Replicas
	}

	// We fill up the pod descriptor list with SSTemplatePods for all ordinals.
	podDescList := make(PodDescriptorList, numWantPods)
	for i := 0; i < int(numWantPods); i++ {
		// If possible, try to preserve the labels and annotations of the actual
		// pod. This is important because the label "controller-revision-hash"
		// contains the "version" of the pod as assigned to by its owner
		// statefulset and this label is, of course, not contained in the labels
		// of the pod template. Without that label contained in the labels of
		// the newly created pod, that pod will always be recreated on creation
		// of a new statefulset => We don't want that.
		var labels, annotations map[string]string
		pod, err := cli.cliSet.CoreV1().Pods(ss.GetNamespace()).Get(ctx, fmt.Sprintf("%s-%d", ss.GetName(), i), metav1.GetOptions{})
		switch {
		case err == nil:
			labels = pod.GetLabels()
			annotations = pod.GetAnnotations()
		case errors.IsNotFound(err):
			// If the pod does not exist, we can't get its labels. That's ok. We
			// will return the labels of the pod template.
		case err != nil:
			return nil, fmt.Errorf("could not get pod %s-%d: %w", ss.GetName(), i, err)
		}

		podDescList[i] = SSTemplatePod{
			ss:          ss,
			ordinal:     i,
			labels:      labels,
			annotations: annotations,
		}
	}
	return podDescList, nil
}

func (cli *K8sCli) instanceList(ctx context.Context, labelValuesComponent []string, labelValueTier, namespace, release string) (*corev1.PodList, error) {
	selector, err := constructSelector(labelValuesComponent, []string{release}, []string{labelValueTier})
	if err != nil {
		return nil, fmt.Errorf("could not construct selector: %w", err)
	}
	opts := metav1.ListOptions{
		LabelSelector: selector.String(),
	}
	return cli.cliSet.CoreV1().Pods(namespace).List(ctx, opts)
}

func (cli *K8sCli) DeletePodBlocking(ctx context.Context, namespace, name string) error {
	if err := cli.cliSet.CoreV1().Pods(namespace).Delete(ctx, name, metav1.DeleteOptions{}); err != nil {
		// If the pod, we want to delete, does not exist, we are done.
		if errors.IsNotFound(err) {
			return nil
		}
		// Otherwise we return the error.
		return fmt.Errorf("could not delete pod %s: %w", name, err)
	}
	if err := waitTillGone(fmt.Sprintf("pod %s", name), func() error {
		_, err := cli.cliSet.CoreV1().Pods(namespace).Get(ctx, name, metav1.GetOptions{})
		return err
	}); err != nil {
		return err
	}
	return nil
}

func (cli *K8sCli) findPVWithPGVersion(ctx context.Context, namespace, name, currentVersion string) (corev1.PersistentVolume, error) {
	nameReq, err := labels.NewRequirement(labelKeyOriginPVCName, selection.Equals, []string{name})
	if err != nil {
		return corev1.PersistentVolume{}, fmt.Errorf("could not construct name requirement: %w", err)
	}
	namespaceReq, err := labels.NewRequirement(labelKeyOriginPVCNamespace, selection.Equals, []string{namespace})
	if err != nil {
		return corev1.PersistentVolume{}, fmt.Errorf("could not construct namespace requirement: %w", err)
	}
	pgVersionReq, err := labels.NewRequirement(labelKeyPGVersion, selection.Equals, []string{currentVersion})
	if err != nil {
		return corev1.PersistentVolume{}, fmt.Errorf("could not construct postgres version requirement: %w", err)
	}
	selectorStr := labels.NewSelector().Add(*nameReq, *namespaceReq, *pgVersionReq).String()
	pvList, err := cli.cliSet.CoreV1().PersistentVolumes().List(ctx, metav1.ListOptions{
		LabelSelector: selectorStr,
	})
	if err != nil {
		return corev1.PersistentVolume{}, fmt.Errorf("could not list persistent volumes: %w", err)
	}
	if len(pvList.Items) == 0 {
		return corev1.PersistentVolume{}, fmt.Errorf("could not find persistent volume using label selector %s", selectorStr)
	}
	if len(pvList.Items) > 1 {
		return corev1.PersistentVolume{}, fmt.Errorf("found more than one persistent volume using label selector %s", selectorStr)
	}
	return pvList.Items[0], nil
}

func constructSelector(labelValuesComponent, labelValuesRelease, labelValuesTier []string) (labels.Selector, error) {
	componentReq, err := labels.NewRequirement(labelKeyComponent, selection.In, labelValuesComponent)
	if err != nil {
		return nil, fmt.Errorf("could not construct component requirement: %w", err)
	}
	releaseReq, err := labels.NewRequirement(labelKeyRelease, selection.In, labelValuesRelease)
	if err != nil {
		return nil, fmt.Errorf("could not construct release requirement: %w", err)
	}
	tierReq, err := labels.NewRequirement(labelKeyTier, selection.In, labelValuesTier)
	if err != nil {
		return nil, fmt.Errorf("could not construct tier requirement: %w", err)
	}
	selector := labels.NewSelector()
	selector = selector.Add(*componentReq, *releaseReq, *tierReq)
	return selector, nil
}

func (cli *K8sCli) PublicInstanceList(ctx context.Context, namespace, release string) (*corev1.PodList, error) {
	return cli.instanceList(ctx, []string{instanceTypePublic}, appTier, namespace, release)
}

func (cli *K8sCli) AuthorInstanceList(ctx context.Context, namespace, release string) (*corev1.PodList, error) {
	return cli.instanceList(ctx, []string{instanceTypeAuthor}, appTier, namespace, release)
}

func (cli *K8sCli) DBInstanceList(ctx context.Context, dbNames []string, namespace, release string) (*corev1.PodList, error) {
	return cli.instanceList(ctx, dbNames, databaseTier, namespace, release)
}

func (cli *K8sCli) StatefulSetList(ctx context.Context, components, tiers []string, namespace, release string) (*appsv1.StatefulSetList, error) {
	selector, err := constructSelector(components, []string{release}, tiers)
	if err != nil {
		return nil, fmt.Errorf("could not construct selector: %w", err)
	}
	opts := metav1.ListOptions{
		LabelSelector: selector.String(),
	}
	return cli.cliSet.AppsV1().StatefulSets(namespace).List(ctx, opts)
}

var errNumReplicasAlreadyCorrect = fmt.Errorf("number of replicas is already correct")

func retryFunc(taskName string, retryFunc func() error, maxElapsedTime ...time.Duration) error {
	defaultMaxElapsedTime := time.Minute
	if len(maxElapsedTime) != 1 {
		maxElapsedTime = []time.Duration{defaultMaxElapsedTime}
	}

	// Don't retry for too long.
	bo := backoff.NewExponentialBackOff()
	bo.MaxElapsedTime = maxElapsedTime[0]
	return backoff.Retry(errorLogWrapper(taskName, retryFunc), bo)
}

func (cli *K8sCli) DeleteStatfulSetWithOrphanedPods(ctx context.Context, namespace, name string) error {
	return retryFunc(fmt.Sprintf("deleting statefulset %s with orphaned pods", name), func() error {
		pp := propagationPolicyOrphan
		err := cli.cliSet.AppsV1().StatefulSets(namespace).Delete(ctx, name, metav1.DeleteOptions{
			PropagationPolicy: &pp,
		})
		// We are done if the stateful set has successfully been deleted or if
		// it does not exist anymore.
		if err == nil || errors.IsNotFound(err) {
			return nil
		}
		return fmt.Errorf("could not delete stateful set with orphaned pods: %w", err)
	})
}

func (cli *K8sCli) ScaleStatefulSetTo(ctx context.Context, namespace, name string, wantReplicas int32) error {
	return retryFunc(fmt.Sprintf("scaling statefulset %s to %d replicas", name, wantReplicas), func() error {
		// Get the stateful set.
		ss, err := cli.cliSet.AppsV1().StatefulSets(namespace).Get(ctx, name, metav1.GetOptions{})
		if err != nil {
			return fmt.Errorf("could not get stateful set: %w", err)
		}
		if ss == nil {
			return fmt.Errorf("stateful set pointer cannot be nil")
		}

		// Check how many replicas we have.
		haveReplicas := int32(1)
		if ss.Spec.Replicas != nil {
			haveReplicas = *ss.Spec.Replicas
		}

		// If we have the correct number of replicas already, we are done.
		if haveReplicas == wantReplicas {
			return errSuccess{errNumReplicasAlreadyCorrect}
		}

		// Otherwise we update the stateful set.
		ss.Spec.Replicas = &wantReplicas
		_, err = cli.cliSet.AppsV1().StatefulSets(namespace).Update(ctx, ss, metav1.UpdateOptions{})
		if err != nil {
			return fmt.Errorf("could not update stateful set: %w", err)
		}

		return nil
	})
}

var errReclaimPolicyAlreadyCorrect = fmt.Errorf("reclaim policy is already correct")

func (cli *K8sCli) PatchReclaimPolicyWithRetry(ctx context.Context, pvName string, reclaimPolicy corev1.PersistentVolumeReclaimPolicy, addLabels map[string]string) error {
	return retryFunc(fmt.Sprintf("patch persistent volume %s's reclaim policy to %s", pvName, reclaimPolicy), func() error {
		// Get the persistent volume.
		pv, err := cli.cliSet.CoreV1().PersistentVolumes().Get(ctx, pvName, metav1.GetOptions{})
		if err != nil {
			return fmt.Errorf("could not get persistent volume: %w", err)
		}
		if pv == nil {
			return fmt.Errorf("persistent volume pointer cannot be nil")
		}

		// If we have the correct reclaim policy already, we are done.
		if pv.Spec.PersistentVolumeReclaimPolicy == reclaimPolicy {
			return errSuccess{errReclaimPolicyAlreadyCorrect}
		}

		// Otherwise we update the persistent volume.
		if pv.Labels == nil {
			pv.Labels = make(map[string]string)
		}
		for k, v := range addLabels {
			pv.Labels[k] = v
		}
		pv.Spec.PersistentVolumeReclaimPolicy = reclaimPolicy
		_, err = cli.cliSet.CoreV1().PersistentVolumes().Update(ctx, pv, metav1.UpdateOptions{})
		if err != nil {
			return fmt.Errorf("could not update persistent volume: %w", err)
		}

		return nil
	})
}

var errClaimRefAlreadyEmpty = fmt.Errorf("claim ref is already empty")

func (cli *K8sCli) RemoveClaimRefWithRetry(ctx context.Context, name string) error {
	return retryFunc(fmt.Sprintf("remove persistent volume %s's claim ref", name), func() error {
		// Get the persistent volume.
		pv, err := cli.cliSet.CoreV1().PersistentVolumes().Get(ctx, name, metav1.GetOptions{})
		if err != nil {
			return fmt.Errorf("could not get persistent volume: %w", err)
		}
		if pv == nil {
			return fmt.Errorf("persistent volume pointer cannot be nil")
		}

		// If the claim ref is already empty, we are done.
		if pv.Spec.ClaimRef == nil {
			return errSuccess{errClaimRefAlreadyEmpty}
		}

		// Otherwise we update the persistent volume.
		pv.Spec.ClaimRef = nil
		_, err = cli.cliSet.CoreV1().PersistentVolumes().Update(ctx, pv, metav1.UpdateOptions{})
		if err != nil {
			return fmt.Errorf("could not update persistent volume: %w", err)
		}

		return nil
	})
}

// CreatePVCWithRetry creates a pvc and retries in case of an error.
func (cli *K8sCli) CreatePVCWithRetry(ctx context.Context, pvc *corev1.PersistentVolumeClaim, waitTillBound bool) error {
	if pvc == nil {
		return fmt.Errorf("persistent volume claim pointer cannot be nil")
	}

	// Create the pvc.
	if err := retryFunc(fmt.Sprintf("creating pvc %s", pvc.Name), func() error {
		_, err := cli.cliSet.CoreV1().PersistentVolumeClaims(pvc.Namespace).Create(ctx, pvc, metav1.CreateOptions{})
		return err
	}); err != nil {
		return err
	}

	// If we don't have to wait until the pvc is bound, we can return
	// immediately.
	if !waitTillBound {
		return nil
	}

	// Otherwise we wait until the pvc is bound.
	return retryFunc(fmt.Sprintf("waiting for pvc %s to be bound", pvc.Name), func() error {
		pvc, err := cli.cliSet.CoreV1().PersistentVolumeClaims(pvc.Namespace).Get(ctx, pvc.Name, metav1.GetOptions{})
		if err != nil {
			return err
		}
		if pvc == nil {
			return fmt.Errorf("persistent volume claim pointer cannot be nil")
		}
		if pvc.Status.Phase != corev1.ClaimBound {
			return fmt.Errorf("pvc %s is not bound yet", pvc.Name)
		}
		return nil
	}, 5*time.Minute)
}

// CreatePodWithRetry creates a pod and retries in case of an error.
func (cli *K8sCli) CreatePodWithRetry(ctx context.Context, pod *corev1.Pod) error {
	if pod == nil {
		return fmt.Errorf("pod pointer cannot be nil")
	}
	return retryFunc(fmt.Sprintf("creating pod %s", pod.Name), func() error {
		_, err := cli.cliSet.CoreV1().Pods(pod.Namespace).Create(ctx, pod, metav1.CreateOptions{})
		return err
	})
}

// GetStatefulSetWithRetry gets a stateful set and retries in case of an error.
func (cli *K8sCli) GetStatefulSetWithRetry(ctx context.Context, namespace, name string, maxElapsedTime ...time.Duration) (*appsv1.StatefulSet, error) {
	var ss *appsv1.StatefulSet
	err := retryFunc(fmt.Sprintf("getting stateful set %s", name), func() error {
		var err error
		ss, err = cli.cliSet.AppsV1().StatefulSets(namespace).Get(ctx, name, metav1.GetOptions{})
		return err
	}, maxElapsedTime...)
	return ss, err
}

// GetPodWithRetry gets a pod and retries in case of an error.
func (cli *K8sCli) GetPodWithRetry(ctx context.Context, namespace, name string, maxElapsedTime ...time.Duration) (*corev1.Pod, error) {
	var pod *corev1.Pod
	err := retryFunc(fmt.Sprintf("getting pod %s", name), func() error {
		var err error
		pod, err = cli.cliSet.CoreV1().Pods(namespace).Get(ctx, name, metav1.GetOptions{})
		return err
	}, maxElapsedTime...)
	return pod, err
}

func (cli *K8sCli) CreateStatefulSetWithRetry(ctx context.Context, ss *appsv1.StatefulSet, maxElapsedTime ...time.Duration) error {
	// Copy only some metadata and the specs to recreate the stateful sets.
	recreateSS := &appsv1.StatefulSet{
		ObjectMeta: metav1.ObjectMeta{
			Name:      ss.Name,
			Namespace: ss.Namespace,
			Labels:    ss.Labels,
		},
		Spec: ss.Spec,
	}
	return retryFunc(fmt.Sprintf("creating stateful set %s", recreateSS.Name), func() error {
		_, err := cli.cliSet.AppsV1().StatefulSets(recreateSS.Namespace).Create(ctx, recreateSS, metav1.CreateOptions{})
		return err
	}, maxElapsedTime...)
}

// Switch volumes switches the persistent volume of the old pvc replacing it by
// the persistent volume of the new pvc (or if this is nil, by the persistent
// volume given with newPvName). During that process the given labels will be
// added to the old and new persistent volume. Note: After a successful switch
// the persistent volume of the old claim, will have the reclaim policy
// "Retain".
func (cli *K8sCli) SwitchVolumes(ctx context.Context, oldPVC corev1.PersistentVolumeClaim, newPVC *corev1.PersistentVolumeClaim, newPvName string, oldPvAddLabels, newPvAddLabels map[string]string) error {
	// Make sure that the SwitchVolumes arguments are "sane".
	if newPVC != nil {
		switch {
		case newPVC.Spec.VolumeName != newPvName:
			return fmt.Errorf("if newPVC pointer is given, argument newPvName name must be %s", newPVC.Spec.VolumeName)
		case newPvName == "":
			newPvName = newPVC.Spec.VolumeName
		}
	}

	// Patch old pv reclaim policy (and add labels).
	if err := cli.PatchReclaimPolicyWithRetry(ctx, oldPVC.Spec.VolumeName, corev1.PersistentVolumeReclaimRetain, oldPvAddLabels); err != nil {
		return fmt.Errorf("could not patch old pv reclaim policy: %w", err)
	}

	// Delete old pvc.
	if err := cli.cliSet.CoreV1().PersistentVolumeClaims(oldPVC.Namespace).Delete(ctx, oldPVC.Name, metav1.DeleteOptions{}); err != nil {
		return fmt.Errorf("could not delete old pvc: %w", err)
	}
	// Wait until it is gone.
	if err := waitTillGone(fmt.Sprintf("old pvc %s", oldPVC.Name), func() error {
		_, err := cli.cliSet.CoreV1().PersistentVolumeClaims(oldPVC.Namespace).Get(ctx, oldPVC.Name, metav1.GetOptions{})
		return err
	}); err != nil {
		return err
	}

	// Patch new pv reclaim policy (and add labels).
	if err := cli.PatchReclaimPolicyWithRetry(ctx, newPvName, corev1.PersistentVolumeReclaimRetain, newPvAddLabels); err != nil {
		return fmt.Errorf("could not patch new pv reclaim policy: %w", err)
	}

	// If there is a new pvc.
	if newPVC != nil {
		// Delete the new pvc.
		if err := cli.cliSet.CoreV1().PersistentVolumeClaims(newPVC.Namespace).Delete(ctx, newPVC.Name, metav1.DeleteOptions{}); err != nil {
			return fmt.Errorf("could not delete new pvc: %w", err)
		}
		// And wait until it is gone.
		if err := waitTillGone(fmt.Sprintf("new pvc %s", newPVC.Name), func() error {
			_, err := cli.cliSet.CoreV1().PersistentVolumeClaims(newPVC.Namespace).Get(ctx, newPVC.Name, metav1.GetOptions{})
			return err
		}); err != nil {
			return err
		}
	}

	// Remove claim ref from new pv.
	if err := cli.RemoveClaimRefWithRetry(ctx, newPvName); err != nil {
		return fmt.Errorf("could not remove claim ref from new pv: %w", err)
	}

	// Create new pvc with same name, labelset and specs as old PVC (important
	// for StatefulSet mapping), but use the new persistent volume.
	updatedPVC := corev1.PersistentVolumeClaim{
		ObjectMeta: metav1.ObjectMeta{
			Namespace: oldPVC.Namespace,
			Name:      oldPVC.Name,
			Labels:    oldPVC.Labels,
		},
		Spec: corev1.PersistentVolumeClaimSpec{
			AccessModes:      oldPVC.Spec.AccessModes,
			Resources:        oldPVC.Spec.Resources,
			StorageClassName: oldPVC.Spec.StorageClassName,
			VolumeMode:       oldPVC.Spec.VolumeMode,
			VolumeName:       newPvName,
		},
	}
	waitTillBound := true
	if err := cli.CreatePVCWithRetry(ctx, &updatedPVC, waitTillBound); err != nil {
		return fmt.Errorf("could not create pvc %s: %w", updatedPVC.Name, err)
	}

	// Reset new pv reclaim policy to delete.
	if err := cli.PatchReclaimPolicyWithRetry(ctx, newPvName, corev1.PersistentVolumeReclaimDelete, nil); err != nil {
		return fmt.Errorf("could not patch new pv reclaim policy to delete: %w", err)
	}

	return nil
}

func (cli *K8sCli) CreatePodFromDescriptor(ctx context.Context, podDesc PodDescriptor) error {
	// Define the pod by copying the specs, namespace, name, labels &
	// annotations from the pod descriptor.
	pod := &corev1.Pod{
		ObjectMeta: metav1.ObjectMeta{
			Namespace:   podDesc.GetNamespace(),
			Name:        podDesc.GetName(),
			Labels:      podDesc.GetLabels(),
			Annotations: podDesc.GetAnnotations(),
		},
		Spec: podDesc.GetSpec(),
	}

	return retryFunc(fmt.Sprintf("creating pod %s from descriptor", pod.Name), func() error {
		if _, err := cli.cliSet.CoreV1().Pods(pod.Namespace).Create(ctx, pod, metav1.CreateOptions{}); err != nil {
			return err
		}
		return nil
	})
}

func isPodReady(pod corev1.Pod) bool {
	for _, cond := range pod.Status.Conditions {
		if cond.Type == corev1.PodReady && cond.Status == corev1.ConditionTrue {
			return true
		}
	}
	return false
}

func (cli *K8sCli) waitTillPodReady(ctx context.Context, namespace, podName string) error {
	return retryFunc(fmt.Sprintf("waiting for pod %s to be ready", podName), func() error {
		pod, err := cli.cliSet.CoreV1().Pods(namespace).Get(ctx, podName, metav1.GetOptions{})
		if err != nil {
			return fmt.Errorf("could not get pod %s: %w", podName, err)
		}
		if pod == nil {
			return fmt.Errorf("pod pointer cannot be nil")
		}
		if !isPodReady(*pod) {
			return fmt.Errorf("pod %s is not ready yet", podName)
		}
		return nil
	}, 24*time.Hour) // Wait for at most 24 hours.
}
