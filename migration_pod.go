package main

import (
	"context"
	"fmt"
	"path/filepath"
	"strings"
	"sync"
	"time"

	"github.com/sirupsen/logrus"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/selection"
)

const (
	keyBindCompleted     = "pv.kubernetes.io/bind-completed"
	keyBoundByController = "pv.kubernetes.io/bound-by-controller"

	oldDbVolumeName = "old-db"
	newDbVolumeName = "new-db"

	oldMountPath = "/old"
	newMountPath = "/new"

	labelKeyOriginPVCNamespace = "origin-pvc-namespace"
	labelKeyOriginPVCName      = "origin-pvc-name"
	labelKeyPGVersion          = "postgres-version"

	migrateContainerName = "migrate"
)

type MigrationPod struct {
	ctx            context.Context
	k8sCli         *K8sCli
	oldPVCDesc     PVCDescriptor
	oldPVC         corev1.PersistentVolumeClaim
	migrationPod   *corev1.Pod
	havePGVersion  int
	wantPGVersion  int
	wantPGTag      string
	migrationImage string
	dbSubPath      string
}

func NewMigrationPod(ctx context.Context, k8sCli *K8sCli, oldPVCDesc PVCDescriptor, haveVersion, wantVersion int, targetTag, migrationImage, dbSubPath string) (MigrationPod, error) {
	// Get the pvc name for the old pod.
	pvcName := oldPVCDesc.GetName()

	// Get the pvc of the old pod.
	oldPVC, err := k8sCli.cliSet.CoreV1().PersistentVolumeClaims(oldPVCDesc.GetNamespace()).Get(ctx, pvcName, metav1.GetOptions{})
	if err != nil {
		return MigrationPod{}, fmt.Errorf("could not get old data volume claim for pod %s: %w", oldPVCDesc.GetName(), err)
	}
	if oldPVC == nil {
		return MigrationPod{}, fmt.Errorf("pvc pointer cannot be nil")
	}

	return MigrationPod{
		ctx:            ctx,
		k8sCli:         k8sCli,
		oldPVCDesc:     oldPVCDesc,
		oldPVC:         *oldPVC,
		havePGVersion:  haveVersion,
		wantPGVersion:  wantVersion,
		migrationImage: migrationImage,
		dbSubPath:      dbSubPath,
		wantPGTag:      targetTag,
	}, nil
}

func (p *MigrationPod) Prepare() error {
	// Create a pvc for the new database.
	newPVC, err := p.newDataVolumeClaim(p.oldPVC)
	if err != nil {
		return fmt.Errorf("could not construct new data volume claim: %w", err)
	}

	// Configure the migration pod.
	postgresUserID := int64(70)
	rootUserID := int64(0)
	p.migrationPod = &corev1.Pod{
		ObjectMeta: metav1.ObjectMeta{
			Namespace: p.oldPVC.Namespace,
			Name:      fmt.Sprintf("migrate-%s", p.oldPVC.Name),
			Labels:    newPVC.Labels,
		},
		Spec: corev1.PodSpec{
			RestartPolicy: corev1.RestartPolicyNever,
			Volumes: []corev1.Volume{
				{
					VolumeSource: corev1.VolumeSource{
						PersistentVolumeClaim: &corev1.PersistentVolumeClaimVolumeSource{
							ClaimName: p.oldPVC.Name,
						},
					},
					Name: oldDbVolumeName,
				},
				{
					VolumeSource: corev1.VolumeSource{
						PersistentVolumeClaim: &corev1.PersistentVolumeClaimVolumeSource{
							ClaimName: newPVC.Name,
						},
					},
					Name: newDbVolumeName,
				},
			},
			InitContainers: []corev1.Container{
				{
					Name:    "own-new-volume",
					Image:   "busybox:1.36",
					Command: []string{"sh", "-c", "chgrp 70 /new && chmod 775 /new"},
					VolumeMounts: []corev1.VolumeMount{
						{
							Name:      newDbVolumeName,
							MountPath: newMountPath,
						},
					},
					SecurityContext: &corev1.SecurityContext{
						RunAsUser: &rootUserID,
					},
				},
				{
					Name:    "backup-hba-file",
					Image:   "busybox:1.36",
					Command: []string{"sh", "-c", "cat /old/data/pg_hba.conf && cp /old/data/pg_hba.conf /new/pg_hba.conf"},
					VolumeMounts: []corev1.VolumeMount{
						{
							Name:      oldDbVolumeName,
							MountPath: oldMountPath,
						},
						{
							Name:      newDbVolumeName,
							MountPath: newMountPath,
						},
					},
					SecurityContext: &corev1.SecurityContext{
						RunAsUser:  &postgresUserID,
						RunAsGroup: &postgresUserID,
					},
				},
			},
			Containers: []corev1.Container{
				{
					Name:  migrateContainerName,
					Image: p.migrationImage,
					Command: []string{
						"/app", commandNameMigrate,
						fmt.Sprintf("--%s", flagNameTo), fmt.Sprintf("%d", p.wantPGVersion),
						fmt.Sprintf("--%s", flagNameOldPgDataPath), filepath.Join(oldMountPath, p.dbSubPath),
						fmt.Sprintf("--%s", flagNameNewPgDataPath), filepath.Join(newMountPath, p.dbSubPath),
					},
					VolumeMounts: []corev1.VolumeMount{
						{
							Name:      oldDbVolumeName,
							MountPath: oldMountPath,
						},
						{
							Name:      newDbVolumeName,
							MountPath: newMountPath,
						},
					},
					SecurityContext: &corev1.SecurityContext{
						RunAsUser:  &postgresUserID,
						RunAsGroup: &postgresUserID,
					},
				},
			},
		},
	}

	// Make sure that the migration pod does not exist yet.
	hasPod, err := p.k8sCli.HasPod(p.ctx, p.migrationPod.Name, p.migrationPod.Namespace, p.migrationPod.Labels)
	if err != nil {
		return fmt.Errorf("could not check whether migration pod %s is already existing: %w", p.migrationPod.Name, err)
	}

	// If the migration pod already exists, delete it.
	if hasPod {
		if err := p.k8sCli.cliSet.CoreV1().Pods(p.migrationPod.Namespace).Delete(p.ctx, p.migrationPod.Name, metav1.DeleteOptions{}); err != nil {
			return fmt.Errorf("could not delete outdated migration pod %s: %w", p.migrationPod.Name, err)
		}
	}

	// Make sure that the new PVC does not exist yet.
	hasPVC, err := p.k8sCli.HasPVC(p.ctx, newPVC.Name, newPVC.Namespace, newPVC.Labels)
	if err != nil {
		return fmt.Errorf("could not check whether pvc %s is already existing: %w", newPVC.Name, err)
	}

	// If the new pvc already exists, delete it.
	if hasPVC {
		if err := p.k8sCli.cliSet.CoreV1().PersistentVolumeClaims(newPVC.Namespace).Delete(p.ctx, newPVC.Name, metav1.DeleteOptions{}); err != nil {
			return fmt.Errorf("could not delete outdated \"new\" data volume claim %s: %w", newPVC.Name, err)
		}
	}

	// Create the new pvc (with retry).
	waitTillBound := false
	if err := p.k8sCli.CreatePVCWithRetry(p.ctx, newPVC, waitTillBound); err != nil {
		return err
	}

	return nil
}

func (p *MigrationPod) CreatePods() error {
	if p.migrationPod == nil {
		return fmt.Errorf("migration pod pointer cannot be nil")
	}
	oldDbPod := p.oldPVCDesc.dbPod
	oldAppPod := p.oldPVCDesc.appPod
	// If there is was no old db pod when we started migrating, we don't need to
	// create it again.
	if oldDbPod == nil {
		return nil
	}

	// Define the new db pod by copying the specs, namespace, name, labels &
	// annotations from the old db pod.
	newDbPod := &corev1.Pod{
		ObjectMeta: metav1.ObjectMeta{
			Namespace:   oldDbPod.GetNamespace(),
			Name:        oldDbPod.GetName(),
			Labels:      oldDbPod.GetLabels(),
			Annotations: oldDbPod.GetAnnotations(),
		},
		Spec: oldDbPod.GetSpec(),
	}

	// Update the postgres container image tag in the pod spec.
	for i := range newDbPod.Spec.Containers {
		if strings.HasPrefix(newDbPod.Spec.Containers[i].Image, postgresImageRepository) {
			newDbPod.Spec.Containers[i].Image = fmt.Sprintf("%s:%s", postgresImageRepository, p.wantPGTag)
		}
	}

	// Create the new db pod.
	if err := p.k8sCli.CreatePodWithRetry(p.ctx, newDbPod); err != nil {
		return fmt.Errorf("could not create new db pod: %w", err)
	}

	// Recreate the old app pod (if there was one).
	if oldAppPod != nil {
		if err := p.k8sCli.CreatePodFromDescriptor(p.ctx, oldAppPod); err != nil {
			return fmt.Errorf("could not recreate app pod: %w", err)
		}
	}

	// Wait until the new db pod is ready.
	if err := p.k8sCli.waitTillPodReady(p.ctx, newDbPod.Namespace, newDbPod.Name); err != nil {
		return fmt.Errorf("could not wait for new db pod to be ready: %w", err)
	}

	// Wait until the recreated app pod is ready (if there is one).
	if oldAppPod != nil {
		if err := p.k8sCli.waitTillPodReady(p.ctx, oldAppPod.GetNamespace(), oldAppPod.GetName()); err != nil {
			return fmt.Errorf("could not wait for recreated app pod to be ready: %w", err)
		}
	}

	return nil
}

func (p *MigrationPod) DeletePods() error {
	if p.migrationPod == nil {
		return fmt.Errorf("migration pod pointer cannot be nil")
	}
	oldDbPod := p.oldPVCDesc.dbPod
	oldAppPod := p.oldPVCDesc.appPod
	// If there is was no old db pod when we started migrating, we don't need to
	// delete it.
	if oldDbPod == nil {
		return nil
	}

	// Delete the db pod and wait until it is gone.
	if err := p.k8sCli.DeletePodBlocking(p.ctx, oldDbPod.GetNamespace(), oldDbPod.GetName()); err != nil {
		return fmt.Errorf("could not delete old db pod: %w", err)
	}

	// Delete the app pod (if there is one) and wait until it is gone.
	if oldAppPod != nil {
		if err := p.k8sCli.DeletePodBlocking(p.ctx, oldAppPod.GetNamespace(), oldAppPod.GetName()); err != nil {
			return fmt.Errorf("could not delete old app pod: %w", err)
		}
	}
	return nil
}

func (p *MigrationPod) Run() error {
	if p.migrationPod == nil {
		return fmt.Errorf("migration pod pointer cannot be nil")
	}

	// Create the migration pod (i.e. run the migration).
	var err error
	p.migrationPod, err = p.k8sCli.cliSet.CoreV1().Pods(p.migrationPod.Namespace).Create(p.ctx, p.migrationPod, metav1.CreateOptions{})
	if err != nil {
		return fmt.Errorf("could not create migration pod: %w", err)
	}

	// Wait for the run of the migration pod to be done.
	return waitTillDone(p.ctx, p.k8sCli, *p.migrationPod, migrateContainerName)
}

func waitTillGone(resourceName string, getFunc func() error) error {
	return retryFunc(fmt.Sprintf("waiting till %s is gone", resourceName), func() error {
		err := getFunc()
		switch {
		case err == nil:
			return fmt.Errorf("%s is still there", resourceName)
		case errors.IsNotFound(err):
			return nil // Success: resource is gone.
		default:
			return fmt.Errorf("could not get %s: %w", resourceName, err)
		}
	})
}

func createPVLabels(pvc corev1.PersistentVolumeClaim, postgresVersion int) map[string]string {
	out := make(map[string]string)
	for k, v := range pvc.Labels {
		out[k] = v
	}
	out[labelKeyOriginPVCNamespace] = pvc.Namespace
	out[labelKeyOriginPVCName] = pvc.Name
	out[labelKeyPGVersion] = fmt.Sprintf("%d", postgresVersion)
	return out
}

func (p *MigrationPod) SwitchVolumes() error {
	// Delete Migration pod first (otherwise the pvcs cannot be deleted!)
	err := p.k8sCli.cliSet.CoreV1().Pods(p.migrationPod.Namespace).Delete(p.ctx, p.migrationPod.Name, metav1.DeleteOptions{})
	if err != nil {
		return fmt.Errorf("could not delete migration pod: %w", err)
	}
	// And wait until it is gone.
	if err := waitTillGone(fmt.Sprintf("migration pod %s", p.migrationPod.Name), func() error {
		_, err := p.k8sCli.cliSet.CoreV1().Pods(p.migrationPod.Namespace).Get(p.ctx, p.migrationPod.Name, metav1.GetOptions{})
		return err
	}); err != nil {
		return err
	}

	// Get the names of the old & new pvc from the migrator pod specs.
	var newPVCName string
	for _, vol := range p.migrationPod.Spec.Volumes {
		if vol.Name == newDbVolumeName {
			newPVCName = vol.PersistentVolumeClaim.ClaimName
		}
	}

	// Get the new pvc.
	newPVC, err := p.k8sCli.cliSet.CoreV1().PersistentVolumeClaims(p.oldPVC.Namespace).Get(p.ctx, newPVCName, metav1.GetOptions{})
	if err != nil {
		return fmt.Errorf("could not get new pvc: %w", err)
	}
	if newPVC == nil {
		return fmt.Errorf("new pvc pointer cannot be nil")
	}

	// Create the pv labels.
	oldPvLabels := createPVLabels(p.oldPVC, p.havePGVersion)
	newPvLabels := createPVLabels(*newPVC, p.wantPGVersion)

	// Switch the volumes.
	if err := p.k8sCli.SwitchVolumes(p.ctx, p.oldPVC, newPVC, newPVC.Spec.VolumeName, oldPvLabels, newPvLabels); err != nil {
		return err
	}
	return nil
}

// waitTillDone waits until the pod with given name is done. If a containerName
// is given, the pod is also considered as done if only the specified container
// has terminated.
func waitTillDone(ctx context.Context, k8sCli *K8sCli, pod corev1.Pod, containerName string) error {
	// Compile label selector from migration pod labels.
	selector, err := compileLabelSelector(pod)
	if err != nil {
		return fmt.Errorf("could not compile label selector: %w", err)
	}

	var wg sync.WaitGroup
	var migrationPodErr error
	wg.Add(1)
	go func() {
		for {
			select {
			// Stop if ctx is done.
			case <-ctx.Done():
				return
			default:
			}

			var stopWatching bool
			stopWatching, migrationPodErr = watchPod(ctx, k8sCli, pod.Namespace, pod.Name, containerName, selector)
			// Stop if required.
			if stopWatching {
				wg.Done()
				return
			}
			// Otherwise continue watching, but log the error.
			logrus.Error(migrationPodErr.Error())

			// Wait a bit before restarting watchers.
			time.Sleep(time.Second)
		}
	}()

	// Wait until the watcher goroutine is done an return the error of the
	// migration pod execution.
	wg.Wait()
	return migrationPodErr
}

// watchPod returns true if we can stop watching the pod with given name and an
// error if the pod has not succeeded. If a non empty containerName is given, we
// also stop watching still running pods for which the specified container has
// terminated.
func watchPod(ctx context.Context, k8sCli *K8sCli, namespace, name, containerName string, selector labels.Selector) (bool, error) {
	// Create a watcher that watches all pods having the given label selector
	// and name.
	watcher, err := k8sCli.cliSet.CoreV1().Pods(namespace).Watch(ctx, metav1.ListOptions{
		LabelSelector: selector.String(),
		FieldSelector: fmt.Sprintf("metadata.name=%s", name), // https://github.com/kubernetes/kubernetes/issues/53459
	})
	if err != nil {
		return false, fmt.Errorf("could not watch pods with label selector %s: %w", selector.String(), err)
	}
	logrus.Infof("starting watching pod")
	defer func() {
		watcher.Stop()
		logrus.Info("stop watching pod")
	}()

	for {
		select {
		// Stop if parent ctx is done.
		case <-ctx.Done():
			return true, ctx.Err()
		case event, more := <-watcher.ResultChan():
			if !more {
				// Channel is closed. We should restart the watchers.
				return false, fmt.Errorf("event channel of watcher has been closed")
			}
			logrus.Infof("encountered event %s for pod %s", event.Type, name)
			pod, err := k8sCli.cliSet.CoreV1().Pods(namespace).Get(ctx, name, metav1.GetOptions{})
			if err != nil {
				return false, fmt.Errorf("could not get pod %s: %w", name, err)
			}
			logrus.Debugf("pod %s is currently in phase %s", name, pod.Status.Phase)
			switch pod.Status.Phase {
			// The pod was successful.
			case corev1.PodSucceeded:
				return true, nil
			// The pod failed.
			case corev1.PodFailed:
				return true, fmt.Errorf("pod failed (check pod %s logs for details)", name)
			case corev1.PodRunning:
				for _, containerStatus := range pod.Status.ContainerStatuses {
					if containerStatus.Name != containerName {
						continue
					}
					if containerStatus.State.Terminated == nil {
						continue
					}
					if containerStatus.State.Terminated.ExitCode == 0 {
						// The container terminated successful.
						return true, nil
					}
					// The container terminated with an error.
					return true, fmt.Errorf("container %s failed (check pod %s logs for details)", containerName, name)
				}
				// Our container is still running => we keep on watching.
			default:
				// For all other phases we keep on watching.
			}
		}
	}

}

func compileLabelSelector(pod corev1.Pod) (labels.Selector, error) {
	selector := labels.NewSelector()
	for k, v := range pod.Labels {
		r, err := labels.NewRequirement(k, selection.Equals, []string{v})
		if err != nil {
			return selector, fmt.Errorf("could not create label requirement for label %s: %w", k, err)
		}
		if r == nil {
			return selector, fmt.Errorf("label requirement pointer for label %s is nil", k)
		}
		selector = selector.Add(*r)
	}
	return selector, nil
}

func (p *MigrationPod) newDataVolumeClaim(oldPVC corev1.PersistentVolumeClaim) (*corev1.PersistentVolumeClaim, error) {
	// The new PVC annotations are the old ones without the "bind completed" &
	// "bound by controller" annotations.
	newAnnotations := make(map[string]string)
	for k, v := range oldPVC.Annotations {
		newAnnotations[k] = v
	}
	delete(newAnnotations, keyBindCompleted)
	delete(newAnnotations, keyBoundByController)

	// The new PVC annotations are the old ones but with a different tier.
	newLabels := make(map[string]string)
	for k, v := range oldPVC.Labels {
		newLabels[k] = v
	}
	newLabels[labelKeyTier] = dbMigrationTier

	out := &corev1.PersistentVolumeClaim{
		ObjectMeta: metav1.ObjectMeta{
			Namespace:   oldPVC.Namespace,
			Name:        fmt.Sprintf("new-%s", oldPVC.Name),
			Annotations: newAnnotations,
			Labels:      newLabels,
		},
		Spec: corev1.PersistentVolumeClaimSpec{
			AccessModes:      oldPVC.Spec.AccessModes,
			Resources:        oldPVC.Spec.Resources,
			StorageClassName: oldPVC.Spec.StorageClassName,
			// Intentionally don't copy VolumeMode & VolumeName from the old
			// PVC.
		},
	}
	return out, nil
}
