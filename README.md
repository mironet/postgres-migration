# Postgres-migration tool

This postgres-migration tool has mainly been created to migrate data volumes for
[magnolia-helm](https://gitlab.com/mironet/magnolia-helm) deployments in an
automated fashion - using helm hooks.

In these hooks we call the `migrator` and `rollback` commands of this project.

The `label-pv` and `migrate` commands are used by futher pods that can be spun
up inside the pod running the `migrator` command.

## Migrate Command

The `migrate` command is not coupled to a k8s or magnolia context and can
potentially be used in other contexts as well to migrate any postgres database.

### Example

0. Build the docker image containing the migration program written in go.

    ```bash
    make build-docker
    ```

1. Make sure you start from scratch.

    ```bash
    make down-postgres-new down-postgres down clean-postgres-volumes
    ```

2. Make sure there is some postgres 11 db dump stored under
   `./docker/publicdump.sql`.

3. Run

    ```bash
    make up-postgres
    ```

4. Stop the container with CTRL+C once postgres has been successfully started.

5. Run

    ```bash
    make up
    ```

6. Run

    ```bash
    make up-postgres-new
    ```

### Expected result

The `postgres-data` volume should now contain a `mydata` directory with postgres
11 data from the `./docker/publicdump.sql` dump and a `mydata.new` directory
with equivalent postgres 15 data.

Note: That for the migration, in step 5, the go program defined in `main.go` is
being used.
