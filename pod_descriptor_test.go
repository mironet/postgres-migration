package main

import (
	"reflect"
	"testing"

	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func TestSSTemplatePod_GetSpec(t *testing.T) {
	type fields struct {
		ss      appsv1.StatefulSet
		ordinal int
	}
	tests := []struct {
		name   string
		fields fields
		want   corev1.PodSpec
	}{
		{
			name: "success - append",
			fields: fields{
				ss: appsv1.StatefulSet{
					ObjectMeta: metav1.ObjectMeta{
						Name: "bla-bla-bla",
					},
					Spec: appsv1.StatefulSetSpec{
						ServiceName: "my-service",
						Template: corev1.PodTemplateSpec{
							Spec: corev1.PodSpec{
								Volumes: []corev1.Volume{},
							},
						},
						VolumeClaimTemplates: []corev1.PersistentVolumeClaim{
							{
								ObjectMeta: metav1.ObjectMeta{
									Name: "data",
								},
							},
						},
					},
				},
				ordinal: 17,
			},
			want: corev1.PodSpec{
				Volumes: []corev1.Volume{
					{
						Name: "data",
						VolumeSource: corev1.VolumeSource{
							PersistentVolumeClaim: &corev1.PersistentVolumeClaimVolumeSource{
								ClaimName: "data-bla-bla-bla-17",
							},
						},
					},
				},
				Hostname:  "bla-bla-bla-17",
				Subdomain: "my-service",
			},
		},
		{
			name: "success - replace",
			fields: fields{
				ss: appsv1.StatefulSet{
					ObjectMeta: metav1.ObjectMeta{
						Name: "bla-bla-bla",
					},
					Spec: appsv1.StatefulSetSpec{
						ServiceName: "my-service",
						Template: corev1.PodTemplateSpec{
							Spec: corev1.PodSpec{
								Volumes: []corev1.Volume{
									{
										Name: "data",
									},
								},
							},
						},
						VolumeClaimTemplates: []corev1.PersistentVolumeClaim{
							{
								ObjectMeta: metav1.ObjectMeta{
									Name: "data",
								},
							},
						},
					},
				},
				ordinal: 17,
			},
			want: corev1.PodSpec{
				Volumes: []corev1.Volume{
					{
						Name: "data",
						VolumeSource: corev1.VolumeSource{
							PersistentVolumeClaim: &corev1.PersistentVolumeClaimVolumeSource{
								ClaimName: "data-bla-bla-bla-17",
							},
						},
					},
				},
				Hostname:  "bla-bla-bla-17",
				Subdomain: "my-service",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := SSTemplatePod{
				ss:      tt.fields.ss,
				ordinal: tt.fields.ordinal,
			}
			if got := p.GetSpec(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("SSTemplatePod.GetSpec() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_trimOrdinal(t *testing.T) {
	tests := []struct {
		name string
		want string
	}{
		{
			name: "success",
			want: "success",
		},
		{
			name: "success-0",
			want: "success",
		},
		{
			name: "success-48465312",
			want: "success",
		},
		{
			name: "suc-cess-14",
			want: "suc-cess",
		},
		{
			name: "12",
			want: "12",
		},
		{
			name: "12-13",
			want: "12",
		},
		{
			name: "suc-cess",
			want: "suc-cess",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := trimOrdinal(tt.name); got != tt.want {
				t.Errorf("trimOrdinal() = %v, want %v", got, tt.want)
			}
		})
	}
}
