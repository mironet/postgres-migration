package main

import (
	"fmt"
	"log"
	"os"

	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
)

var (
	version = "v0.0.1"
)

const (
	commandNameMigrate  = "migrate"
	commandNameMigrator = "migrator"
	commandNameRollback = "rollback"
	commandNameLabelPV  = "label-pv"

	flagNameTo            = "to"
	flagNameAuthorTag     = "author-db-tag"
	flagNamePublicTag     = "public-db-tag"
	flagNameSharedTag     = "shared-db-tag"
	flagNameOldPgDataPath = "pg-data"
	flagNameNewPgDataPath = "new-pg-data"

	flagNameNamespace = "namespace"
	flagNamePVCName   = "pvc-name"
)

type migrateArgs struct {
	to            int
	oldPgDataPath string
	newPgDataPath string
}

func (a migrateArgs) validate() error {
	if a.to <= min(supportedVersions) {
		return fmt.Errorf("target version %d to low", a.to)
	}
	if a.to > max(supportedVersions) {
		return fmt.Errorf("target version %d to high", a.to)
	}
	if a.oldPgDataPath == "" {
		return fmt.Errorf("old pg data path cannot be empty")
	}
	if a.newPgDataPath == "" {
		return fmt.Errorf("new pg data path cannot be empty")
	}
	return nil
}

type migratorArgs struct {
	authorDBName    string
	publicDBName    string
	sharedDBName    string
	authorDBSubPath string
	publicDBSubPath string
	sharedDBSubPath string
	namespace       string
	release         string
	app             string
	authorTo        int
	publicTo        int
	sharedTo        int
	authorTag       string
	publicTag       string
	sharedTag       string
	jobName         string
}

func (a *migratorArgs) setPostgresVersions() error {
	var err error

	// If we do a migration for the author db, we need to know the author db
	// target postgres version.
	if a.authorDBName != "" {
		a.authorTo, err = postgresVersionFromImageTag(a.authorTag)
		if err != nil {
			return fmt.Errorf("could not get author postgres version: %w", err)
		}
	}

	// If we do a migration for the public db, we need to know the public db
	// target postgres version.
	if a.publicDBName != "" {
		a.publicTo, err = postgresVersionFromImageTag(a.publicTag)
		if err != nil {
			return fmt.Errorf("could not get public postgres version: %w", err)
		}
	}

	// If we do a migration for the shared db, we need to know the shared db
	// target postgres version.
	if a.sharedDBName != "" {
		a.sharedTo, err = postgresVersionFromImageTag(a.sharedTag)
		if err != nil {
			return fmt.Errorf("could not get shared postgres version: %w", err)
		}
	}

	return nil
}

func (a migratorArgs) validate() error {
	// Check general flags.
	if a.jobName == "" {
		return fmt.Errorf("job name cannot be empty")
	}
	if a.namespace == "" {
		return fmt.Errorf("namespace cannot be empty")
	}
	if a.release == "" {
		return fmt.Errorf("release cannot be empty")
	}
	if a.app == "" {
		return fmt.Errorf("app cannot be empty")
	}

	// Check author related flags, if required.
	if a.authorDBName == "" {
		logrus.Warnf("migrator args has empty author db name, is this correct?")
	}
	if a.authorDBName != "" {
		if a.authorDBSubPath == "" {
			return fmt.Errorf("author db subpath cannot be empty")
		}
		if a.authorTo < min(supportedVersions) {
			return fmt.Errorf("target author version %d to low", a.authorTo)
		}
		if a.authorTo > max(supportedVersions) {
			return fmt.Errorf("target author version %d to high", a.authorTo)
		}
	}

	// Check public related flags, if required.
	if a.publicDBName == "" {
		logrus.Warnf("migrator args has empty public db name, is this correct?")
	}
	if a.publicDBName != "" {
		if a.publicDBSubPath == "" {
			return fmt.Errorf("public db subpath cannot be empty")
		}
		if a.publicTo < min(supportedVersions) {
			return fmt.Errorf("target public version %d to low", a.publicTo)
		}
		if a.publicTo > max(supportedVersions) {
			return fmt.Errorf("target public version %d to high", a.publicTo)
		}
	}

	// Check shared db related flags, if required.
	if a.sharedDBName != "" {
		if a.sharedDBSubPath == "" {
			return fmt.Errorf("shared db subpath cannot be empty")
		}
		if a.sharedTo < min(supportedVersions) {
			return fmt.Errorf("target shared version %d to low", a.sharedTo)
		}
		if a.sharedTo > max(supportedVersions) {
			return fmt.Errorf("target shared version %d to high", a.sharedTo)
		}
	}

	return nil
}

type labelPVArgs struct {
	namespace     string
	pvcName       string
	oldPgDataPath string
}

func (a labelPVArgs) validate() error {
	if a.namespace == "" {
		return fmt.Errorf("namespace cannot be empty")
	}
	if a.pvcName == "" {
		return fmt.Errorf("pvc name cannot be empty")
	}
	if a.oldPgDataPath == "" {
		return fmt.Errorf("old pg data path cannot be empty")
	}
	return nil
}

func main() {
	logrus.Infof("running version %s", version)
	var migrateArgs migrateArgs
	var migratorArgs migratorArgs
	var rollbackArgs rollbackArgs
	var labelPVArgs labelPVArgs
	var debug bool
	app := &cli.App{}
	app.Name = "magnolia-psql-migrator"
	app.Usage = "A CLI tool to migrate magnolia postgres databases. Used e.g. by magnolia-helm chart in its pg migration hook."
	app.Flags = []cli.Flag{
		&cli.BoolFlag{
			Name: "debug", Aliases: []string{"d"}, Value: false, Usage: "Activate debug logging.",
			Destination: &debug, EnvVars: []string{},
		},
	}

	migrateFlags := []cli.Flag{
		&cli.IntFlag{
			Name: flagNameTo, Aliases: nil, Value: 0, Usage: fmt.Sprintf("Postgres (major) version targeted. Must be > %d and <= %d", min(supportedVersions), max(supportedVersions)),
			Destination: &migrateArgs.to, EnvVars: []string{},
		},
		&cli.StringFlag{
			Name: flagNameOldPgDataPath, Aliases: nil, Value: "", Usage: "Filepath to the old postgres data directory.",
			Destination: &migrateArgs.oldPgDataPath, EnvVars: []string{},
		},
		&cli.StringFlag{
			Name: flagNameNewPgDataPath, Aliases: nil, Value: "", Usage: "Filepath to the new postgres data directory.",
			Destination: &migrateArgs.newPgDataPath, EnvVars: []string{},
		},
	}

	rollbackFlags := []cli.Flag{
		&cli.StringFlag{
			Name: "namespace", Aliases: nil, Value: "", Usage: "Specify the k8s namespace. Only statefulsets in this namespace will be rolled back.",
			Destination: &rollbackArgs.namespace, EnvVars: []string{},
		},
		&cli.StringFlag{
			Name: "release", Aliases: nil, Value: "", Usage: "Specify the magnolia release name. Only statefulsets for this release name will be rolled back.",
			Destination: &rollbackArgs.release, EnvVars: []string{},
		},
		&cli.StringFlag{
			Name: "author-db-name", Aliases: nil, Value: "", Usage: "(Optional) Specify the name of the author db. It corresponds to helm value magnoliaAuthor.db.name. If not specified, the author db will not be rolled back.",
			Destination: &rollbackArgs.authorDBName, EnvVars: []string{},
		},
		&cli.StringFlag{
			Name: "public-db-name", Aliases: nil, Value: "", Usage: "(Optional) Specify the name of the public db(s). It corresponds to helm value magnoliaPublic.db.name. If not specified, the public db will not be rolled back.",
			Destination: &rollbackArgs.publicDBName, EnvVars: []string{},
		},
		&cli.StringFlag{
			Name: "shared-db-name", Aliases: nil, Value: "", Usage: "(Optional) Specify the name of the shared db. It corresponds to helm value sharedDb.db.name. If not specified, the shared db will not be rolled back.",
			Destination: &rollbackArgs.sharedDBName, EnvVars: []string{},
		},
	}

	migratorFlags := []cli.Flag{
		&cli.StringFlag{
			Name: "author-db-name", Aliases: nil, Value: "", Usage: "(Optional) Specify the name of the author db. It corresponds to helm value magnoliaAuthor.db.name. If not specified, the author db will not be migrated.",
			Destination: &migratorArgs.authorDBName, EnvVars: []string{},
		},
		&cli.StringFlag{
			Name: "public-db-name", Aliases: nil, Value: "", Usage: "(Optional) Specify the name of the public db(s). It corresponds to helm value magnoliaPublic.db.name. If not specified, the public db will not be migrated.",
			Destination: &migratorArgs.publicDBName, EnvVars: []string{},
		},
		&cli.StringFlag{
			Name: "shared-db-name", Aliases: nil, Value: "", Usage: "(Optional) Specify the name of the shared db. It corresponds to helm value sharedDb.db.name. If not specified, the shared db will not be migrated.",
			Destination: &migratorArgs.sharedDBName, EnvVars: []string{},
		},
		&cli.StringFlag{
			Name: "author-db-sub-path", Aliases: nil, Value: "data", Usage: "The subpath to the postgres data directory in the author's data volume. It corresponds to helm value magnoliaAuthor.db.persistence.subPath.",
			Destination: &migratorArgs.authorDBSubPath, EnvVars: []string{},
		},
		&cli.StringFlag{
			Name: "public-db-sub-path", Aliases: nil, Value: "data", Usage: "The subpath to the postgres data directory in the public's data volume. It corresponds to helm value magnoliaPublic.db.persistence.subPath.",
			Destination: &migratorArgs.publicDBSubPath, EnvVars: []string{},
		},
		&cli.StringFlag{
			Name: "shared-db-sub-path", Aliases: nil, Value: "data", Usage: "The subpath to the postgres data directory in the sharedDB's data volume. It corresponds to helm value sharedDb.db.persistence.subPath.",
			Destination: &migratorArgs.sharedDBSubPath, EnvVars: []string{},
		},
		&cli.StringFlag{
			Name: "namespace", Aliases: nil, Value: "", Usage: "Specify the k8s namespace. Only volumes in this namespace will be migrated.",
			Destination: &migratorArgs.namespace, EnvVars: []string{},
		},
		&cli.StringFlag{
			Name: "release", Aliases: nil, Value: "", Usage: "Specify the magnolia release name. Only volumes for this release name will be migrated.",
			Destination: &migratorArgs.release, EnvVars: []string{},
		},
		&cli.StringFlag{
			Name: "app", Aliases: nil, Value: "", Usage: "Specify the app name (i.e. the value of the \"app\" label of the data pvc(s)). Only volumes for this app will be migrated.",
			Destination: &migratorArgs.app, EnvVars: []string{},
		},
		&cli.StringFlag{
			Name: flagNameAuthorTag, Aliases: nil, Value: "", Usage: "The db tag of the author db. It corresponds to helm value magnoliaAuthor.db.tag.",
			Destination: &migratorArgs.authorTag, EnvVars: []string{},
		},
		&cli.StringFlag{
			Name: flagNamePublicTag, Aliases: nil, Value: "", Usage: "The db tag of the public db(s). It corresponds to helm value magnoliaPublic.db.tag.",
			Destination: &migratorArgs.publicTag, EnvVars: []string{},
		},
		&cli.StringFlag{
			Name: flagNameSharedTag, Aliases: nil, Value: "", Usage: "The db tag of the shared db. It corresponds to helm value sharedDb.db.tag.",
			Destination: &migratorArgs.sharedTag, EnvVars: []string{},
		},
		&cli.StringFlag{
			Name: "job-name", Aliases: nil, Value: "postgres-migration", Usage: "The name of the k8s job this migrator command is run in.",
			Destination: &migratorArgs.jobName, EnvVars: []string{},
		},
	}

	labelPVFlags := []cli.Flag{
		&cli.StringFlag{
			Name: flagNameNamespace, Aliases: nil, Value: "", Usage: fmt.Sprintf("Specify the k8s namespace of the pvc referred to by flag %s.", flagNamePVCName),
			Destination: &labelPVArgs.namespace, EnvVars: []string{},
		},
		&cli.StringFlag{
			Name: flagNamePVCName, Aliases: nil, Value: "", Usage: "Specify the name of the pvc for which the bound pv should be labeled.",
			Destination: &labelPVArgs.pvcName, EnvVars: []string{},
		},
		&cli.StringFlag{
			Name: flagNameOldPgDataPath, Aliases: nil, Value: "", Usage: "The subpath to the postgres data directory in the data volume. It corresponds to helm value {magnoliaAuthor|magnoliaPublic|sharedDb}.db.persistence.subPath.",
			Destination: &labelPVArgs.oldPgDataPath, EnvVars: []string{},
		},
	}

	app.Commands = []*cli.Command{
		{
			Name:  commandNameMigrate,
			Usage: "Run the migration.",
			Flags: migrateFlags,
			Action: func(ctx *cli.Context) error {
				setLogLevel(debug)
				if err := migrateArgs.validate(); err != nil {
					return fmt.Errorf("invalid argument: %w", err)
				}
				return migrate(migrateArgs)
			},
		},
		{
			Name:  commandNameMigrator,
			Usage: "Prepare and run magnolia db migration on a k8s cluster.",
			Flags: migratorFlags,
			Action: func(ctx *cli.Context) error {
				setLogLevel(debug)
				if err := migratorArgs.setPostgresVersions(); err != nil {
					return fmt.Errorf("could not set postgres versions: %w", err)
				}
				if err := migratorArgs.validate(); err != nil {
					return fmt.Errorf("invalid argument: %w", err)
				}
				return migrator(ctx.Context, migratorArgs)
			},
		},
		{
			Name:  commandNameRollback,
			Usage: "Rollback a magnolia db migration on a k8s cluster. This will make sure that all PVCs refer to the PVs containing the data that was used before the failed migration attempt.",
			Flags: rollbackFlags,
			Action: func(ctx *cli.Context) error {
				setLogLevel(debug)
				if err := rollbackArgs.validate(); err != nil {
					return fmt.Errorf("invalid argument: %w", err)
				}
				return rollback(ctx.Context, rollbackArgs)
			},
		},
		{
			Name:  commandNameLabelPV,
			Usage: "Label a PV with the postgres version that is stored on it.",
			Flags: labelPVFlags,
			Action: func(ctx *cli.Context) error {
				setLogLevel(debug)
				if err := labelPVArgs.validate(); err != nil {
					return fmt.Errorf("invalid argument: %w", err)
				}
				return labelPV(ctx.Context, labelPVArgs)
			},
		},
	}
	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}

func setLogLevel(debug bool) {
	if debug {
		logrus.SetLevel(logrus.DebugLevel)
	}
}
