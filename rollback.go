package main

import (
	"context"
	"fmt"

	"github.com/sirupsen/logrus"
	"golang.org/x/sync/errgroup"
	appsv1 "k8s.io/api/apps/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type rollbackArgs struct {
	namespace    string
	release      string
	authorDBName string
	publicDBName string
	sharedDBName string
}

func (a rollbackArgs) validate() error {
	if a.namespace == "" {
		return fmt.Errorf("namespace cannot be empty")
	}
	if a.release == "" {
		return fmt.Errorf("release cannot be empty")
	}
	return nil
}

func rollback(ctx context.Context, args rollbackArgs) error {
	// Get a k8s client.
	k8sCli, err := k8sCli()
	if err != nil {
		return fmt.Errorf("could not get k8s client: %w", err)
	}

	// Rollback author, public & shared db components one after another (if required).
	argsList := []rollbackModeArgs{
		{
			componentName: "author",
			dbName:        args.authorDBName,
			appComponent:  instanceTypeAuthor,
		},
		{
			componentName: "public",
			dbName:        args.publicDBName,
			appComponent:  instanceTypePublic,
		},
		{
			componentName: "sharedDB",
			dbName:        args.sharedDBName,
			appComponent:  "",
		},
	}
	g, gCtx := errgroup.WithContext(ctx)
	errMap := make(map[string]error, 0)
	for _, rollbackComponentArgs := range argsList {
		rollbackArgs := rollbackComponentArgs
		g.Go(func() error {
			err := rollbackMode(gCtx, k8sCli, args.namespace, args.release, rollbackArgs)
			errMap[rollbackArgs.componentName] = err
			return err
		})
	}
	waitErr := g.Wait()
	if waitErr != nil {
		for componentName, err := range errMap {
			if err != nil {
				logrus.Errorf("could not rollback %s: %v", componentName, err)
			}
		}
	}
	return waitErr
}

type rollbackModeArgs struct {
	componentName string
	dbName        string
	appComponent  string
}

func rollbackMode(ctx context.Context, k8sCli *K8sCli, namespace, release string, args rollbackModeArgs) error {
	if args.dbName != "" {
		// Fetch  stateful sets (db & app) and rollback dbs.
		appSS, dbSS, err := fetchStatefulSets(ctx, k8sCli, namespace, release, args.appComponent, args.dbName)
		if err != nil {
			return fmt.Errorf("could not fetch %s stateful sets: %w", args.componentName, err)
		}
		if err := rollbackByStatefulSets(ctx, k8sCli, appSS, dbSS); err != nil {
			return fmt.Errorf("could not rollback %s db: %w", args.componentName, err)
		}
		logrus.Infof("successfully rollbacked %s db", args.componentName)
	}
	return nil
}

func sortedPVCDescriptorList(ctx context.Context, k8sCli *K8sCli, appSS *appsv1.StatefulSet, dbSS *appsv1.StatefulSet) ([]PVCDescriptor, error) {
	out := make([]PVCDescriptor, 0)

	// Get db and app pod lists.
	dbPodList, err := k8sCli.sortedPodListFromStatefulset(ctx, *dbSS)
	if err != nil {
		return out, fmt.Errorf("could not get db pod list: %w", err)
	}
	var appPodList PodDescriptorList
	if appSS != nil {
		var err error
		appPodList, err = k8sCli.sortedPodListFromStatefulset(ctx, *appSS)
		if err != nil {
			return out, fmt.Errorf("could not get app pod list: %w", err)
		}
	}

	for i, dbPod := range dbPodList {
		// Find the corresponding app pod to the current db pod (if there is
		// one).
		var appPod PodDescriptor
		if i < len(appPodList) {
			appPod = appPodList[i]
		}
		out = append(out, PVCDescriptor{
			dbPod:  dbPod,
			appPod: appPod,
		})
	}
	return out, nil
}

func rollbackByStatefulSets(ctx context.Context, k8sCli *K8sCli, appSS *appsv1.StatefulSet, dbSS *appsv1.StatefulSet) error {
	if dbSS == nil {
		return fmt.Errorf("db stateful set pointer cannot be nil")
	}

	pvcList, err := sortedPVCDescriptorList(ctx, k8sCli, appSS, dbSS)
	if err != nil {
		return fmt.Errorf("could not get sorted pvc descriptor list: %w", err)
	}

	// Define a variable to keep book of whether stateful sets must be
	// recreated at the end of this function.
	mustRecreateStatefulSets := false

	// Loop through all db pods in reverse order. Why reverse order? Since we
	// run the rollback in a post-rollback hook, the db pods are generally
	// already beeing updated by the rollbacked stateful set while we are
	// running this code here. The stateful sets do their rolling upgrade in
	// reverse ordinal order. Thus, if we do it the same way here, we minimize
	// the probability of all pods of the stateful set being updated at the same
	// time.
	for i := len(pvcList) - 1; i >= 0; i-- {
		logrus.Infof("found pvc descriptor %s", pvcList[i])
		dbPod := pvcList[i].dbPod
		appPod := pvcList[i].appPod

		// Fetch the current postgres version.
		currentVersion, err := pvcList[i].PostgresVersion()
		if err != nil {
			return fmt.Errorf("could not get current postgres version for pod %s: %w", dbPod.GetName(), err)
		}

		// Get the pvc name for the db pod.
		pvcName := pvcList[i].GetName()

		// Get the pvc and pv of the db pod.
		dataPVC, err := k8sCli.cliSet.CoreV1().PersistentVolumeClaims(dbPod.GetNamespace()).Get(ctx, pvcName, metav1.GetOptions{})
		if err != nil {
			return fmt.Errorf("could not get data volume claim for pod %s: %w", dbPod.GetName(), err)
		}
		if dataPVC == nil {
			return fmt.Errorf("pvc pointer cannot be nil")
		}
		dataPV, err := k8sCli.cliSet.CoreV1().PersistentVolumes().Get(ctx, dataPVC.Spec.VolumeName, metav1.GetOptions{})
		if err != nil {
			return fmt.Errorf("could not get data pv for pod %s: %w", dbPod.GetName(), err)
		}
		if dataPV == nil {
			return fmt.Errorf("data pv for pod %s cannot be nil", dbPod.GetName())
		}

		// If the data pv has no postgres-version label or the label corresponds
		// to the current postgres version.
		if dataPV.Labels == nil ||
			dataPV.Labels[labelKeyPGVersion] == "" ||
			dataPV.Labels[labelKeyPGVersion] == fmt.Sprintf("%d", currentVersion) {
			// We are fine.
			logrus.Infof("no rollback required for pod %s referencing data pv with labels %v", dbPod.GetName(), dataPV.Labels)
			continue
		}
		// Here we know that the data pv has a postgres-version label that
		// corresponds to a postgres version which is different from the current
		// postgres version.

		// Thus there should be a persistent volume on the cluster with the
		// correct postgres-version label. Find it.
		pv, err := k8sCli.findPVWithPGVersion(ctx, dataPVC.Namespace, dataPVC.Name, fmt.Sprintf("%d", currentVersion))
		if err != nil {
			return fmt.Errorf("could not find pv with postgres version %d: %w", currentVersion, err)
		}

		// Delete stateful sets, but not their pods.
		if err := k8sCli.DeleteStatfulSetWithOrphanedPods(ctx, dbSS.Namespace, dbSS.Name); err != nil {
			return fmt.Errorf("could not delete db stateful set %s: %w", dbSS.Name, err)
		}
		if appSS != nil {
			if err := k8sCli.DeleteStatfulSetWithOrphanedPods(ctx, appSS.Namespace, appSS.Name); err != nil {
				return fmt.Errorf("could not delete app stateful set %s: %w", appSS.Name, err)
			}
		}
		// And remember that they must be recreated in the end.
		mustRecreateStatefulSets = true

		// Delete the db pod and wait until it is gone.
		if err := k8sCli.DeletePodBlocking(ctx, dbPod.GetNamespace(), dbPod.GetName()); err != nil {
			return fmt.Errorf("could not delete db pod: %w", err)
		}

		// Delete the app pod (if there is one) and wait until it is gone.
		if appPod != nil {
			if err := k8sCli.DeletePodBlocking(ctx, appPod.GetNamespace(), appPod.GetName()); err != nil {
				return fmt.Errorf("could not delete app pod: %w", err)
			}
		}

		// Set pv as data volume for dataPVC.
		if err := k8sCli.SwitchVolumes(ctx, *dataPVC, nil, pv.Name, nil, nil); err != nil {
			return fmt.Errorf("could not switch volumes: %w", err)
		}

		// Recreate pods.
		if err := k8sCli.CreatePodFromDescriptor(ctx, dbPod); err != nil {
			return fmt.Errorf("could not recreate db pod: %w", err)
		}
		if appPod != nil {
			if err := k8sCli.CreatePodFromDescriptor(ctx, appPod); err != nil {
				return fmt.Errorf("could not recreate app pod: %w", err)
			}
		}

		// Wait until the db pod is ready.
		if err := k8sCli.waitTillPodReady(ctx, dbPod.GetNamespace(), dbPod.GetName()); err != nil {
			return fmt.Errorf("could not wait for db pod to be ready: %w", err)
		}

		// Wait until the app pod is ready (if there is one).
		if appPod != nil {
			if err := k8sCli.waitTillPodReady(ctx, appPod.GetNamespace(), appPod.GetName()); err != nil {
				return fmt.Errorf("could not wait for recreated app pod to be ready: %w", err)
			}
		}
	}

	// Recreate stateful sets if required.
	if mustRecreateStatefulSets {
		if err := k8sCli.CreateStatefulSetWithRetry(ctx, dbSS); err != nil {
			return fmt.Errorf("could not recreate db stateful set: %w", err)
		}
		if appSS != nil {
			if err := k8sCli.CreateStatefulSetWithRetry(ctx, appSS); err != nil {
				return fmt.Errorf("could not recreate app stateful set: %w", err)
			}
		}
	}

	return nil
}
